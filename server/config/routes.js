var usuarios=require('../controllers/usuarios');	
var passport=require('./passport');
var enlaces=require('../controllers/enlaces');
var puntajes=require('../controllers/puntajes');
var etiquetas=require('../controllers/etiquetas');
var evaluaciones=require('../controllers/evaluaciones');
var sesiones=require('../controllers/sesiones');
var competencias=require('../controllers/competencias');
var habilidades=require('../controllers/habilidades');
var historiasMedicas=require('../controllers/historiasMedicas');
var comentariosHistoria=require('../controllers/comentariosHistoria');
var ayudas=require('../controllers/ayudas_auditivas');
var mensajes=require('../controllers/mensajes');
var rehabilitaciones=require('../controllers/rehabilitaciones');
var educativos=require('../controllers/educativos');
var identificaciones=require('../controllers/identificaciones');
var organizaciones=require('../controllers/organizaciones');
var audios=require('../controllers/audios');
var alertas=require('../controllers/alertas');
var avances=require('../controllers/avances');
var auditoria=require('../controllers/auditoria');

module.exports = function(app){

	app.get('/partials/*', function(req, res) {
	  	res.render('../../public/app/' + req.params['0']);
	});

	//login y registro GET & POST
	app.post('/registro',usuarios.registro);

	app.post('/login',usuarios.login);

	app.post('/logout',usuarios.logout);

	app.get('/session',usuarios.userAuthenticated);

	app.get('/auth/twitter',passport.authenticate('twitter'));

	app.get('/auth/twitter/callback',
		passport.authenticate('twitter',{
			successRedirect:'/',
			failureRedirect:'/login'
		}));

	//auditorias GET & POST
	app.post('/nueva_auditoria',auditoria.auditoria_guardar);
	 
	app.post('/auditorias',auditoria.consultar_auditorias);
	
	//Alertas GET & POST
	app.post('/alertas_save',alertas.guardar_alerta);
	
	app.post('/alertas',alertas.getAlertas);
	
	app.post('/alertas_delete',alertas.eliminarAlerta);
	
	//Enlaces GET & POST
	app.post('/enlaces',enlaces.guardar_enlace);

	app.post('/enlaces_eliminar',enlaces.eliminar_enlace);

	app.get('/enlaces',enlaces.getEnlaces);

	//Etiquetas GET & POST
	app.post('/etiquetas',etiquetas.guardar_etiqueta);

	app.post('/etiquetas_eliminar',etiquetas.eliminar_etiqueta);

	app.get('/etiquetas',etiquetas.getEtiquetas);	

	//Etiquetas GET & POST
	app.post('/evaluaciones',evaluaciones.guardar_evaluacion);

	app.post('/evaluaciones_eliminar',evaluaciones.eliminar_evaluacion);

	app.get('/evaluaciones',evaluaciones.getEvaluaciones);	

	//Puntajes GET
	app.post('/puntajes',puntajes.getPuntajes);

	app.post('/puntajes_guardar',puntajes.guardar_puntajes);

	//Sesiones GET
	app.post('/sesiones',sesiones.guardarSesiones);

	app.post('/sesiones_consulta',sesiones.getSesiones);

	//Competencias POST & GET
	app.post('/competencias',competencias.guardar_competencia);

	app.post('/competencias_eliminar',competencias.eliminar_competencia);

	app.get('/competencias',competencias.getCompetencias);

	//Habilidades POST & GET
	app.post('/habilidades',habilidades.guardar_habilidad);

	app.post('/habilidades_eliminar',habilidades.eliminar_habilidad);

	app.get('/habilidades',habilidades.getHabilidades);

	app.get('*', function(req, res) {
	  	res.render('index');
	});

	//Historias GET & POST
	app.post('/historias',historiasMedicas.guardar_historia);

	app.post('/consulta_historia',historiasMedicas.consultar);

	app.post('/historias_ident',usuarios.get_usuario);

	app.post('/comentarios_historia',comentariosHistoria.guardar_comentario);

	app.post('/get_comentarios_historia',comentariosHistoria.get_comentarios);

	//Usuarios GET & POST
	app.post('/usuarios_actualizar',usuarios.actualizar_usuario);

	app.post('/usuarios_habilitar',usuarios.cambiar_habilitar_usuario);

	app.post('/usuarios_consultar',usuarios.get_usuario);

	app.post('/usuarios_getUsersHabilitados',usuarios.get_users_habilitados);

	app.post('/usuarios_consultar2',usuarios.get_usuarioByUserName);

	app.post('/usuarios_consultar_todos',usuarios.get_usuarios);
	
	//Acudientes GET & POST
	app.post('/acudientes_agregar',historiasMedicas.agregar_acudiente);

	app.post('/acudientes_consultar',historiasMedicas.get_acudientes);

	//Ayudas GET & POST
	app.post('/ayudas',ayudas.guardar_ayuda_auditiva);

	app.post('/ayudas_eliminar',ayudas.eliminar_ayuda_auditiva);

	app.post('/ayudas_consulta',ayudas.getayudas_auditivas);	

	//Mensajes POST & GET
	
	app.post('/mensajes',mensajes.guardar_mensaje); 
			
	app.post('/mensajes_consulta',mensajes.getMensajes);
	
	//Rehabilitaciones GET & POST
	app.post('/rehabilitaciones',rehabilitaciones.guardar_rehabilitacion);

	app.post('/rehabilitaciones_eliminar',rehabilitaciones.eliminar_rehabilitacion);

	app.post('/rehabilitaciones_consulta',rehabilitaciones.getrehabilitacion);	

	//Niveles educativos GET & POST
	app.post('/educativos',educativos.guardar_nivel_educativo);

	app.post('/educativos_eliminar',educativos.eliminar_nivel_educativo);

	app.post('/educativos_consulta',educativos.getnivel_educativo);	

	//Identificaciones educativos GET & POST
	app.post('/identificaciones',identificaciones.guardar_identificacion);

	app.post('/identificaciones_eliminar',identificaciones.eliminar_identificacion);

	app.post('/identificaciones_consulta',identificaciones.getidentificacion);

	//Identificaciones educativos GET & POST
	app.post('/organizaciones',organizaciones.guardar_organizacion);

	app.post('/organizaciones_eliminar',organizaciones.eliminar_organizacion);

	app.post('/organizaciones_consulta',organizaciones.getOrganizacion);

	//Biblioteca de audios GET & POST
	app.post('/audios_comentarios',audios.agregar_comentario);

	app.post('/audios_calificacion',audios.agregar_calificacion);

	app.post('/audios_useract',audios.get_audios_useract);

	app.post('/audios_guardar',audios.guardar_audio);

	//Avances Usuario Actividad GET & POST
	app.post('/avances_comentario',avances.comentar);

	app.post('/avances_calificacion',avances.calificar);

	app.post('/avances_usuario',avances.get_avances_usuario);

	app.post('/avances_crear',avances.crear_avance);
		
	app.post('/avances_habilitar',avances.habilitar);

	app.post('/avances_completar',avances.completar);
};