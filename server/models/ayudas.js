var models=require('./models'),
Schema=models.Schema;

var ayudasSchema= new Schema({
	nombre:String,
	fecha:{type:Date, default:Date()}
});

var Ayudas = models.model('Ayuda',ayudasSchema,'ayudas');
module.exports=Ayudas;