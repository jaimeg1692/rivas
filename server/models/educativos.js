var models=require('./models'),
Schema=models.Schema;

var educativosSchema= new Schema({
	nombre:String,
	fecha:{type:Date, default:Date()}
});

var Educativos = models.model('Educativo',educativosSchema,'educativos');
module.exports=Educativos;