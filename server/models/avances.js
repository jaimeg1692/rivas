var models=require('./models'),
Schema=models.Schema;

var avancesSchema= new Schema({
	Usuario:Object,
	Actividad:Object,
	Calificaciones:[{nombreEspecialista:String,valoracion:Number,fecha:Date}],
	Comentarios:[{nombreEspecialista:String,texto:String,fecha:Date}],
	Habilitado:{type:Boolean, default:false},
	Completado:{type:Boolean, default:false},
	fecha:{type:Date, default:Date()}
});

var Avances = models.model('Avance',avancesSchema,'avances');
module.exports=Avances;