var models=require('./models'),
Schema=models.Schema;

var audiosSchema= new Schema({
	nombreUsuario:String,
	nombreArchivo:String,
	nombreActividad:String,
	calificacion:[{nombreEspecialista:String,valoracion:Number,fecha:Date}],
	comentarios:[{nombreEspecialista:String,texto:String,fecha:Date}],
	fecha:{type:Date, default:Date()}
});

var Audios = models.model('Audio',audiosSchema,'audios');
module.exports=Audios;