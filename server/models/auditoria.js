var models=require('./models'),
Schema=models.Schema;

var auditoriaSchema= new Schema({
	tabla:String, 
	cambio:String, 	
	campo:String,
	valor:String,
	cliente:String,
	usuario:String,  
	fecha: {type:Date, default:Date()}
});  

var Auditoria = models.model('Auditoria',auditoriaSchema,'auditorias');
module.exports=Auditoria;