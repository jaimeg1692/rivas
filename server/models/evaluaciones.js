var models=require('./models'),
Schema=models.Schema;

var evaluacionesSchema= new Schema({
	Nombre:String,
	fecha:{type:Date, default:Date()}
});

var Evaluaciones = models.model('Evaluacion',evaluacionesSchema,'evaluaciones');
module.exports=Evaluaciones;