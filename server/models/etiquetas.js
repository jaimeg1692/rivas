var models=require('./models'),
Schema=models.Schema;

var etiquetasSchema= new Schema({
	Nombre:String,
	Comentario:String,
	fecha:{type:Date, default:Date()}
});

var Etiquetas = models.model('Etiqueta',etiquetasSchema,'etiquetas');
module.exports=Etiquetas;