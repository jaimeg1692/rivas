var models=require('./models'),
Schema=models.Schema;

var identificacionesSchema= new Schema({
	nombre:String,
	fecha:{type:Date, default:Date()}
});

var Identificaciones = models.model('Identificacion',identificacionesSchema,'identificaciones');
module.exports=Identificaciones;