var models=require('./models'),
Schema=models.Schema;

var rehabilitacionesSchema= new Schema({
	nombre:String,
	fecha:{type:Date, default:Date()}
});

var Rehabilitaciones = models.model('Rehabilitacion',rehabilitacionesSchema,'rehabilitaciones');
module.exports=Rehabilitaciones;