var models = require('./models'),
Schema=models.Schema;

var usuariosSchema=new Schema({
	nombre:String,
	nombre_usuario:String,
	tipoid:Object,
	identificacion:Number,
	password:String,
	email:String,
	habilitado:{type:Boolean, default:false},
	perfil:{type:String, default:"USER"},
	fecha:{type:Date, default:Date()}
});

usuariosSchema.methods={
	authenticate:function(password){
		return this.password == password;
	}
}

var Usuario = models.model('Usuario',usuariosSchema,'usuarios');
module.exports=Usuario;