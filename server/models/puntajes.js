var models=require('./models'),
Schema=models.Schema;

var puntajesSchema= new Schema({
	nombre:String,
	puntos:Number,
	actividad:String,
	tiempo:Number,
	fecha:{type:Date, default:Date()}
});

var Puntajes = models.model('Puntajes',puntajesSchema,'puntajes');
module.exports = Puntajes;