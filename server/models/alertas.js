var models=require('./models'),
Schema=models.Schema;

var alertasSchema= new Schema({
	usuario:String, 
	doc_usuario:String, 
	tipo_alerta:String,  
	fecha: Date/*{type:Date, default: Date()}*/ 
});  

var Alertas = models.model('Alerta',alertasSchema,'alertas');
module.exports=Alertas;