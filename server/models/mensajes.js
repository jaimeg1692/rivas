var models=require('./models'),
Schema=models.Schema;

var mensajesSchema= new Schema({
	remitente:String,
	destinatario:String,
	asunto:String,
	mensaje:String,
	fecha:{type:Date, default:Date()}
});

var Mensajes = models.model('Mensaje',mensajesSchema,'mensajes');
module.exports=Mensajes;