var models=require('./models'),
Schema=models.Schema;

var organizacionesSchema= new Schema({
	nombre:String,
	fecha:{type:Date, default:Date()}
});

var Organizaciones = models.model('Organizacion',organizacionesSchema,'organizaciones');
module.exports=Organizaciones;