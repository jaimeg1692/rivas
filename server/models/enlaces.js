var models=require('./models'),
Schema=models.Schema;

var enlacesSchema= new Schema({
	Titulo:String,
	Enlace:String,
	Comentario:String,
	Habilidad:Object,
	Competencias:Object,
	Nivel:Number,
	Tipo_eval:Object,
	Puntos:Number,
	fecha:{type:Date, default:Date()}
});

var Enlaces = models.model('Enlace',enlacesSchema,'enlaces');
module.exports=Enlaces;