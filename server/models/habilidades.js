var models=require('./models'),
Schema=models.Schema;

var habilidadSchema= new Schema({
	Nombre:String,
	Comentario:String,
	fecha:{type:Date, default:Date()}
});

var Habilidades = models.model('Habilidad',habilidadSchema,'habilidades');
module.exports=Habilidades;