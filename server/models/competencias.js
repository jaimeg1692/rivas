var models=require('./models'),
Schema=models.Schema;

var competenciasSchema= new Schema({
	Nombre:String,
	Comentario:String,
	fecha:{type:Date, default:Date()}
});

var Competencias = models.model('Competencia',competenciasSchema,'competencias');
module.exports=Competencias;