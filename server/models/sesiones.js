var models=require('./models'),
Schema=models.Schema;

var sesionesSchema= new Schema({ 
	nombreUsuario:String,
	plataforma:{type:String, default: 'Software'},
	fecha: Date /*{type:Date, default:Date()}*/
});

var Sesiones = models.model('Sesion',sesionesSchema,'sesiones');
module.exports=Sesiones;