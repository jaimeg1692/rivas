var Habilidades= require('../models/habilidades');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_habilidad=function(req,res,next){
	var habilidades = new Habilidades({
		Nombre:req.body.nombreHabilidad,
		Comentario:req.body.comentarioHabilidad
	});
	habilidades.save(function(err,habilidad){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, habilidad:habilidades});
		}
	});
};

exports.eliminar_habilidad=function(req,res,next){
	var habilidad=req.body._id;
	var query_d=Habilidades.findOne({_id:habilidad});
	query_d.exec(function(err,doc){
		Habilidades.remove({_id:habilidad},function(err, habilidad){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, habilidad:habilidad});
		}
    	});
	})
}

exports.getHabilidades=function(req,res,next){
	Habilidades.find({})
	.exec(function(err,habilidades){
		if(err){
			console.log(err);
		}else{
			res.send({success: true,habilidad:habilidades});
		}
	});
};