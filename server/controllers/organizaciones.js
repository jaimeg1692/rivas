var Organizaciones= require('../models/organizaciones');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_organizacion=function(req,res,next){
	var organizaciones = new Organizaciones({
		nombre:req.body.nombreOrganizacion,
	});

	organizaciones.save(function(err,organizaciones){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, organizacion:organizaciones});
		}
	});
};

exports.eliminar_organizacion=function(req,res,next){
	var organizacion=req.body._id;
	var query_d=Organizaciones.findOne({_id:organizacion});
	query_d.exec(function(err,doc){
		Organizaciones.remove({_id:organizacion},function(err, organizacion) {
			if(err){
				console.log(err);
				res.send({success:false, message:err});
			}else{
				res.send({success: true});
			}
    	});
	})
}

exports.getOrganizacion=function(req,res,next){
	Organizaciones.find({})
	.exec(function(err,organizaciones){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true,organizacion:organizaciones});
		}
	});
};