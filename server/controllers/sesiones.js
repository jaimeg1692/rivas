var Sesiones= require('../models/sesiones');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardarSesiones=function(req,res,next){
	var usurioSesionNombre=req.session.passport.nombre;
	var sesiones = new Sesiones({
		nombreUsuario:req.body.username,
		fecha: req.body.fecha,
	});
	sesiones.save(function(err,sesion){
		if(err){
			console.log(err);  
			res.send({success:false, message:err});
		}else{
			res.send({success: true, sesion:sesion});
		}
	}); 
};  

exports.getSesiones=function(req,res,next){
	Sesiones.find({})
	.exec(function(err,sesiones){
		if(err){
			console.log(err);
		}else{
			res.send({success:true,sesion:sesiones});
		}
	});
};