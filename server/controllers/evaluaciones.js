var Evaluaciones= require('../models/evaluaciones');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_evaluacion=function(req,res,next){
	var evaluaciones = new Evaluaciones({
		Nombre:req.body.nombreEvaluacion,
	});
	evaluaciones.save(function(err,evaluacion){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, evaluacion:evaluacion});
		}
	});
};

exports.eliminar_evaluacion=function(req,res,next){
	var evaluacion=req.body._id;
	var query_d=Evaluaciones.findOne({_id:evaluacion});
	query_d.exec(function(err,doc){
		Evaluaciones.remove({_id:evaluacion},function(err, evaluacion) {
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true});
		}
    	});
	})
}

exports.getEvaluaciones=function(req,res,next){
	Evaluaciones.find({})
	.exec(function(err,evaluaciones){
		if(err){
			console.log(err);
		}else{
			res.send({success: true,evaluacion:evaluaciones});
		}
	});
};