var ComentarioHistoria = require('../models/comentariosHistoria');
var ObjectId = require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_comentario = function(req, res, next)
{
	var comentario = new ComentarioHistoria({
		Profesional:req.body.profesional,
		Texto:req.body.texto,
		Identificacion:req.body.identificacion
	});
	var query = ComentarioHistoria.findOne({Texto:comentario.Texto});
	query.exec( function( err , doc )
	{
		if( doc == null )
		{
			comentario.save( function ( err , comentario )
			{
				if ( err ) 
				{
					console.log( err );
					res.send({success : false, message : err});
				}
				else
				{
					res.send({success: true, comentario : comentario, exists:false});
				}
			});
		}
	});
}

exports.get_comentarios=function(req,res,next){
	var ident = req.body.identificacion;
	ComentarioHistoria.find({Identificacion:ident})
	.exec(function(err,comentariosHistorias){
		if( err )
		{
			console.log( err );
			res.send({success:false,message:error});
		}
		else
		{
			if( comentariosHistorias!=null )
			{
				res.send({success:true,comentarios:comentariosHistorias});
				console.log(comentariosHistorias);
			}
			else
			{
				res.send({success:false,comentarios:comentariosHistorias});
			}
		}
	});
};