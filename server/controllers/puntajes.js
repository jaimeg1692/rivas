var Puntajes= require('../models/puntajes');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.getPuntajes=function(req,res,next){
	Puntajes.find({nombre:req.body.username})
	.exec(function(err,puntajes){
		if(err){
			console.log(err);
			res.send({success:false});
		}else{
			console.log(puntajes);
			res.send({success:true,puntaje:puntajes});
		}
	});
};

exports.guardar_puntajes=function(req,res,next){
	var puntajes = new Puntajes({
		nombre:req.body.nombreUsuario,
		puntos:req.body.puntos,
		actividad:req.body.nombreActividad,
		tiempo:req.body.tiempo
	});
	puntajes.save(function(err,puntajes){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success:true, puntaje:puntajes});
		}
	});
};