var Etiquetas= require('../models/etiquetas');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_etiqueta=function(req,res,next){
	var etiquetas = new Etiquetas({
		Nombre:req.body.nombreEtiqueta,
		Comentario:req.body.comentarioEtiqueta,
	});
	etiquetas.save(function(err,etiqueta){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, etiqueta:etiqueta});
		}
	});
};

exports.eliminar_etiqueta=function(req,res,next){
	var etiqueta=req.body._id;
	var query_d=Etiquetas.findOne({_id:etiqueta});
	query_d.exec(function(err,doc){
		Etiquetas.remove({_id:etiqueta},function(err, etiqueta){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, etiqueta:etiqueta});
		}
    	});
	})
}

exports.getEtiquetas=function(req,res,next){
	Etiquetas.find({})
	.exec(function(err,etiquetas){
		if(err){
			console.log(err);
		}else{
			res.send({success: true,etiqueta:etiquetas});
		}
	});
};