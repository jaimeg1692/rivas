var Identificaciones= require('../models/identificaciones');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_identificacion=function(req,res,next){
	var ayudas = new Identificaciones({
		nombre:req.body.nombreNivel_educativo,
	});
	ayudas.save(function(err,identificacion){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, identificacion:identificacion});
		}
	});
};

exports.eliminar_identificacion=function(req,res,next){
	var identificacion=req.body._id;
	var query_d=Identificaciones.findOne({_id:identificacion});
	query_d.exec(function(err,doc){
		Identificaciones.remove({_id:identificacion},function(err, identificacion) {
			if(err){
				console.log(err);
				res.send({success:false, message:err});
			}else{
				res.send({success: true});
			}
    	});
	})
}

exports.getidentificacion=function(req,res,next){
	Identificaciones.find({})
	.exec(function(err,identificacion){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true,identificacion:identificacion});
		}
	});
};