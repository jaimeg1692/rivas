var Educativos= require('../models/educativos');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_nivel_educativo=function(req,res,next){
	var educativos = new Educativos({
		nombre:req.body.nombreNivel_educativo,
	});
	educativos.save(function(err,educativo){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, nivel_educativo:educativo});
		}
	});
};

exports.eliminar_nivel_educativo=function(req,res,next){
	var ayuda=req.body._id;
	var query_d=Educativos.findOne({_id:ayuda});
	query_d.exec(function(err,doc){
		Educativos.remove({_id:ayuda},function(err, ayuda) {
			if(err){
				console.log(err);
				res.send({success:false, message:err});
			}else{
				res.send({success: true});
			}
    	});
	})
}

exports.getnivel_educativo=function(req,res,next){
	Educativos.find({})
	.exec(function(err,educativos){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true,nivel_educativo:educativos});
		}
	});
};