var Competencias= require('../models/competencias');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_competencia=function(req,res,next){
	var competencias = new Competencias({
		Nombre:req.body.nombreCompetencia,
		Comentario:req.body.comentarioCompetencia
	});

	competencias.save(function(err,competencia){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, competencia:competencia});
		}
	});
};

exports.eliminar_competencia=function(req,res,next){
	var competencia=req.body._id;
	var query_d=Competencias.findOne({_id:competencia});
	query_d.exec(function(err,doc){
		Competencias.remove({_id:competencia},function(err, competencia){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true});
		}
    	});
	})
}

exports.getCompetencias=function(req,res,next){
	Competencias.find({})
	.exec(function(err,competencias){
		if(err){
			console.log(err);
		}else{
			res.send({success: true,competencia:competencias});
		}
	});
};