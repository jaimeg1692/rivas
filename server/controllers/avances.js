var Avances = require('../models/avances');
var Enlaces = require('../models/enlaces');
var ObjectId = require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.calificar = function(req,res,next)
{
	Avances.findOneAndUpdate({"Usuario.nombre":req.body.nombreUsuario,"Actividad.Titulo":req.body.nombreActividad},
	{$push:
		{Calificaciones:{
			$each: [{nombreEspecialista:req.body.especialista,valoracion:req.body.valoracion,fecha:Date()}]
		}
	}},{safe:true,upsert:true},
	function(error,calif)
	{
		if(error)
		{
			console.log(error);
			res.send({success:false});
		}
		else
		{
			res.send({success:true});
		}

	});
}

exports.habilitar = function(req,res,next)
{
	Avances.findOneAndUpdate({"Usuario.nombre":req.body.nombreUsuario,"Actividad.Titulo":req.body.nombreActividad},
	{$set:{Habilitado:true}},{safe:true,upsert:true},
	function(error,calif)
	{
		if(error)
		{
			console.log(error);
			res.send({success:false});
		}
		else
		{
			res.send({success:true});
		}

	});	
}

exports.completar = function(req,res,next)
{
	Avances.findOneAndUpdate({"Usuario.nombre":req.body.nombreUsuario,"Actividad.Titulo":req.body.nombreActividad},
	{$set:{Completado:true}},{safe:true,upsert:true},
	function(error,calif)
	{
		if(error)
		{
			console.log(error);
			res.send({success:false});
		}
		else
		{
			res.send({success:true});
		}
	});
}

exports.comentar = function(req,res,next)
{
	Avances.findOneAndUpdate({"Usuario.nombre":req.body.nombreUsuario,"Actividad.Titulo":req.body.nombreActividad},
	{$push:
		{Comentarios:{
			$each: [{nombreEspecialista:req.body.especialista,texto:req.body.texto,fecha:Date()}]
		}
	}},{safe:true,upsert:true},
	function(error,calif)
	{
		if(error)
		{
			console.log(error);
			res.send({success:false});
		}
		else
		{
			console.log("Perfecto!!!!!!!!!!!!!!!!!!!!!");
			res.send({success:true});
		}

	});
}

exports.crear_avance=function(req,res,next)
{
	if(req.body.Actividad.Nivel == 1)
	{
		Avances.findOneAndUpdate({"Usuario.nombre":req.body.Usuario.nombre,"Actividad.Titulo":req.body.Actividad.Titulo},
		{$set:{Usuario:req.body.Usuario,Actividad:req.body.Actividad,Habilitado:true}},
		{safe:true,upsert:true},
		function(error,calif)
		{
			if(error)
			{
				console.log(error);
				res.send({success:false});
			}
			else
			{
				res.send({success:true});
			}
		});
	}
	else
	{
		Avances.findOneAndUpdate({"Usuario.nombre":req.body.Usuario.nombre,"Actividad.Titulo":req.body.Actividad.Titulo},
		{$set:{Usuario:req.body.Usuario,Actividad:req.body.Actividad}},
		{safe:true,upsert:true},
		function(error,calif)
		{
			if(error)
			{
				console.log(error);
				res.send({success:false});
			}
			else
			{
				res.send({success:true});
			}
		});
	}
}

exports.get_avances_usuario = function(req,res,next)
{
	var usuario = req.body.nombreUsuario;
	var habilidad = req.body.nombreHabilidad;
	Avances.find({"Usuario.nombre":usuario,"Actividad.Habilidad.Nombre":habilidad})
	.exec(function(error,avancesUsuario)
	{
		if( error )
		{
			console.log(error);
			res.send({success:false,message:error});
		}
		else
		{
			if( avancesUsuario != null )
			{
				res.send({success:true,avances:avancesUsuario});
			}
			else
			{
				res.send({success:false,avances:null});
			}
		}
	});
}