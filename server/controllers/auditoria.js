var Auditorias = require('../models/auditoria');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.auditoria_guardar=function(req,res,next){
	var auditoria = new Auditorias({
		tabla: req.body.tabla,
		cambio: req.body.cambio,
		campo: req.body.campo,
		valor: req.body.valor,
		cliente: req.body.cliente,
		usuario: req.body.usuario,   
		fecha: req.body.fecha,
	});  
	console.log(auditoria); 
	auditoria.save(function(err,auditoria){ 
		if(err){ 
			console.log("err: "+err);
			res.send({success:false, message:err}); 
		}else{
			res.send({success: true, auditoria:auditoria});
		} 
	});
};  

exports.consultar_auditorias=function(req,res,next){
	Auditorias.find({})
	.exec(function(err,auditorias){
		if(err){
			console.log(err);
		}else{
			res.send({success:true,auditoria:auditorias});
		}
	});
};