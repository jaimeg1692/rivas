var Rehabilitaciones= require('../models/rehabilitaciones');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_rehabilitacion=function(req,res,next){
	var rehabilitaciones = new Rehabilitaciones({
		nombre:req.body.nombreRehabilitacion,
	});
	rehabilitaciones.save(function(err,rehabilitacion){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, rehabilitacion:rehabilitacion});
		}
	});
};

exports.eliminar_rehabilitacion=function(req,res,next){
	var ayuda=req.body._id;
	var query_d=Rehabilitaciones.findOne({_id:ayuda});
	query_d.exec(function(err,doc){
		Rehabilitaciones.remove({_id:ayuda},function(err, ayuda) {
			if(err){
				console.log(err);
				res.send({success:false, message:err});
			}else{
				res.send({success: true});
			}
    	});
	})
}

exports.getrehabilitacion=function(req,res,next){
	Rehabilitaciones.find({})
	.exec(function(err,rehabilitacion){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true,rehabilitacion:rehabilitacion});
		}
	});
};