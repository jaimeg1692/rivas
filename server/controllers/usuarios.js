var Usuario = require('../models/usuarios');
var passport = require('../config/passport');

var kickbox = require('kickbox').client('6e4b22f3296bb4518103bfea627f78599bf4c118d94b9cc1fc8bf0e1676f72ee').kickbox();

exports.registro = function(req, res, next){
	var usuario = new Usuario(req.body);
			usuario.save(function (err, usuario){
			if (err) {
				res.send({success : false, message : err});
			}else{

				req.logIn(usuario, function (err){
					if (!err) {
						res.send({logged: true, success: true, usuario : req.session.passport});
					}else{
						console.log(err);
						res.send({logged: false, success: true, usuario : usuario});
					}
				});
			}
		});	
};


exports.login =	function (req, res, next){
	var auth = passport.authenticate('local', function (err, user){
		if (err) {
			console.log(err);
			return next(err);
		}
		if(!user){
			res.send({success : false});
		}
		req.logIn(user, function (err){
			if (err) {
				return next(err)
			}
			res.send({success : true, user : user});
		});
	});
	auth(req, res, next);
};

exports.userAuthenticated = function(req, res, next){
	if (req.isAuthenticated()) {
		res.send({user : req.session.passport, isLogged : true});
	}else{
		res.send({user : {}, isLogged : false});
	}
};


exports.logout = function(req, res, next){
	req.session.destroy(function(err){
		if (!err) {
			res.send({destroy : true});
		}else{
			console.log(err);
		}
	});
};

exports.get_usuario = function(req, res, next){
	var ident = req.body.identificacion;
	Usuario.findOne({identificacion:ident})
	.exec(function(err,usuario){
		if(err){
			console.log(err);
			res.send({success:false,message:error});
		}else{
			if(usuario!=null)
			{
				res.send({success:true,usuario:usuario});
			}
			else
			{
				res.send({success:false,usuario:usuario});
			}
			
		}
	});
};

exports.get_usuarios = function(req, res, next){
		Usuario.find({})
		.exec(function(err,usuario){
			if(err){
				console.log(err);
				res.send({success:false,message:error});
			}else{
					res.send({success:true,usuario:usuario});		
			}
		});
};

exports.actualizar_usuario = function(req,res,next)
{
	var ident = req.body.identificacion;
	Usuario.findOneAndUpdate({identificacion:ident},
		{$set:{habilitado:req.body.habilitado,perfil:req.body.perfil}},
		{safe:true, upsert:true},
		function(err,user)
		{
			if(err)
			{
				console.log(err);
				res.send({success:false});
			}
			else
			{
				res.send({success:true});
			}
		});
};

exports.cambiar_habilitar_usuario = function(req,res,next)
{
	var ident = req.body.identificacion;
	Usuario.findOneAndUpdate({identificacion:ident},
		{$set:{habilitado:req.body.habilitado}},
		{safe:true, upsert:true},
		function(err,user)
		{
			if(err)
			{
				console.log(err);
				res.send({success:false});
			}
			else
			{
				res.send({success:true});
			}
		});
};

exports.get_users_habilitados = function(req,res,next)
{
	Usuario.find({perfil:"USER",habilitado:true})
	.exec(function(error,usuariosHabilitados)
	{
		if( error )
		{
			console.log(error);
			res.send({success:false});
		}
		else
		{
			if( usuariosHabilitados != null )
			{
				res.send({success:true,usuariosHabilitados:usuariosHabilitados});
			}
			else
			{
				res.send({success:false,usuariosHabilitados:null});
			}
		}
	});
};

exports.get_usuarioByUserName = function(req, res, next){
	var ident = req.body.nombre_usuario;
	Usuario.findOne({nombre_usuario:ident})
	.exec(function(err,usuario){
		if(err){
			res.send({success:false,message:error});
			}else{
			if(usuario!=null)
			{
				res.send({success:true,usuario:usuario});
			}
			else
			{
				res.send({success:false,usuario:usuario});
			}
			
		}
	});
};