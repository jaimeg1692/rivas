var Enlaces= require('../models/enlaces');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_enlace=function(req,res,next){
	var enlaces = new Enlaces({
		Titulo:req.body.tituloEnlace,
		Enlace:req.body.enlace,
		Comentario:req.body.comentarioEnlace,
		Habilidad:req.body.habilidadEnlace,
		Competencias:req.body.competenciaEnlace,
		Nivel:req.body.nivelEnlace,
		TipoEval:req.body.tipoEvaluacionEnlace,
		Puntos:req.body.puntosEnlace
	});
	enlaces.save(function(err,enlace){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, enlace:enlace});
		}
	});

};

exports.eliminar_enlace=function(req,res,next){
	var enlace=req.body._id;
	var query_d=Enlaces.findOne({_id:enlace});
	query_d.exec(function(err,doc){
		Enlaces.remove({_id:enlace},function(err, enlace) {
			if(err){
				console.log(err);
				res.send({success:false, message:err});
			}else{
				res.send({success: true});
			}
    	});
	})
}

exports.getEnlaces=function(req,res,next){
	Enlaces.find({})
	.exec(function(err,enlaces){
		if(err){
			console.log(err);
		}else{
			res.send({success: true,enlace:enlaces});
		}
	});
};