var HistoriaMedica = require('../models/historiasMedicas');
var ObjectId = require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_historia = function(req, res, next)
{
	var historia = new HistoriaMedica({
		Nombre:req.body.nombre,
		TipoID:req.body.tipoid,
		Identificacion:req.body.identi,
		FechaNacimiento:req.body.fechanacimiento,
		Edad:req.body.edad,
		Sexo:req.body.sexo,
		Telefono:req.body.telefono,
		Celular:req.body.celular,
		Correo:req.body.correo,
		Ciudad:req.body.ciudad,
		Estrato:req.body.estrato,
		NivelEducativo:req.body.niveleducativo, 
		OidoAyuda:req.body.oidoayuda,
		EdadDiagnosticoPA:req.body.edaddiagnosticopa, 
		TipoAyuda:req.body.tipoayuda,
		FormaUso:req.body.formauso,
		EdadAdaptacion:req.body.edadadaptacion,
		TipoRehab:req.body.tiporehab,
		DiagnosticoAsociado:req.body.diagnosticoasociado,
		CodigoLinguistico:req.body.codigolinguistico,
		FechaCirugia:req.body.fechacirugia, 
		Reimplante:req.body.reimplante,
		FechaConexion:req.body.fechaconexion,  
		Sesiones:req.body.sesiones,
		TiempoRehab:req.body.tiemporehab,
		Interrupciones:req.body.interrupciones,
		LenguajeComprensivo1:req.body.lenguajecomprensivo1,
		LenguajeComprensivo2:req.body.lenguajecomprensivo2,
		LenguajeExpresivo1:req.body.lenguajeexpresivo1,
		LenguajeExpresivo2:req.body.lenguajeexpresivo2,
		LenguajeExpresivo3:req.body.lenguajeexpresivo3,
		Seguimiento: req.body.seguimiento,
		FechaProgramacion: req.body.fechaprogramacion,
		FechaAudiometria: req.body.fechaaudiometria,
		TipoInstEducativa: req.body.tipoinsteducativa,
		NombreInstEducativa: req.body.nombreinsteducativa,
		CompetenciaL1: req.body.competencial1,
		CompetenciaL2: req.body.competencial2,
		CompetenciaL3: req.body.competencial3,
		CompetenciaL4: req.body.competencial4,
		CompetenciaL5: req.body.competencial5,
		CompetenciaL6: req.body.competencial6,
		CompetenciaL7: req.body.competencial7,
		CompetenciaL8: req.body.competencial8,
		CompetenciaM1: req.body.competenciam1,   
		CompetenciaM2: req.body.competenciam2,
		CompetenciaM3: req.body.competenciam3,
		CompetenciaM4: req.body.competenciam4,
		CompetenciaM5: req.body.competenciam5,
		CompetenciaM6: req.body.competenciam6,
		Acudientes:[]
	});


	/*var query = HistoriaMedica.findOne({Identificacion:historia.identificacion});
	query.exec( function( err , doc )
	{
		console.log("EntraUno");
		if( doc == null )
		{
			console.log("EntraDos");*/
			historia.save( function ( err , historia )
			{
				if ( err ) 
				{
					console.log( err );
					res.send({success : false, message : err});
				}
				else
				{
					res.send({success: true, historia : historia, exists:false});
				}
			});
		/*}
	});*/

}

exports.consultar = function(req,res,next)
{
	var ident = req.body.identificacion;
	HistoriaMedica.findOne({Identificacion:ident})
	.exec(function(err,historiaMedica){
		if( err )
		{
			console.log( err );
			res.send({success:false,message:error});
		}
		else
		{
			if( historiaMedica!=null )
			{
				res.send({success:true,historiaMedica:historiaMedica});
			}
			else
			{
				res.send({success:false,historiaMedica:historiaMedica});
			}
			
		}
	});
};

exports.agregar_acudiente = function(req,res,next)
{
	var ident = req.body.identificacion;
	HistoriaMedica.findOneAndUpdate({Identificacion:ident},
	{$push:
		{Acudientes:{
			$each: [{NombreAcudiente:req.body.nombreac,TelefonoAcudiente:req.body.telefonoac,CorreoAcudiente:req.body.correoac,DireccionAcudiente:req.body.direccionac}]
		}
	}},{safe:true, upsert:true},
	function(err,user)
	{
		if(err)
		{
			console.log(err);
			res.send({success:false});
		}
		else
		{
			res.send({success:true,acudiente:{NombreAcudiente:req.body.nombreac,TelefonoAcudiente:req.body.telefonoac,CorreoAcudiente:req.body.correoac,DireccionAcudiente:req.body.direccionac}});
		}
	});
}

exports.get_acudientes = function(req,res,next)
{
	var ident = req.body.identificacion;
	HistoriaMedica.findOne({Identificacion:ident})
	.exec(function(err,historiaMedica){
		if( err )
		{
			console.log( err );
			res.send({success:false,message:error});
		}
		else
		{
			if( historiaMedica!=null )
			{
				res.send({success:true,acudientes:historiaMedica.Acudientes});
			}
			else
			{
				res.send({success:false,acudientes:historiaMedica.Acudientes});
			}
			
		}
	});

}