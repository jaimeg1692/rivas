var Alertas= require('../models/alertas');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_alerta=function(req,res,next){
	var alertas = new Alertas({
		usuario: req.body.usuario, 
		doc_usuario: req.body.doc_usuario,
		tipo_alerta: req.body.tipo_alerta,  
		fecha: req.body.fecha,
	});  
	console.log(alertas); 
	alertas.save(function(err,alerta){ 
		if(err){ 
			console.log("err: "+err);
			res.send({success:false, message:err}); 
		}else{
			res.send({success: true, alerta:alerta});
		} 
	});
};  
 
exports.eliminarAlerta=function(req,res,next){
	var alerta=req.body._id;   
	var query_d=Alertas.findOne({_id:alerta});
	query_d.exec(function(err,doc){
		Alertas.remove({_id:alerta},function(err, alerta) {
		if(err){
			console.log(err);
		}else{
			res.send({success:true});
		}
    	});
	})
}

exports.getAlertas=function(req,res,next){
	Alertas.find({})
	.exec(function(err,alertas){
		if(err){
			console.log(err);
		}else{
			res.send({success:true,alerta:alertas});
			//res.send(alertas);
		}
	});
};