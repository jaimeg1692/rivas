var Mensajes= require('../models/mensajes');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_mensaje=function(req,res,next){
	var mensajes = new Mensajes({
		remitente:req.body.remitenteMensaje,
		destinatario:req.body.destinatarioMensaje,
		asunto:req.body.asuntoMensaje,
		mensaje:req.body.mensajeMensaje
	});
	var query = Mensajes.findOne({mensaje:mensajes.mensaje,destinatario:mensajes.destinatario});
	query.exec( function( err , doc )
	{
		if( doc == null )
		{
			mensajes.save( function ( err , mensaje )
			{
				if ( err ) 
				{
					console.log( err );
					res.send({success:false,message:err});
				}
				else
				{
					console.log(mensaje);
					res.send({success:true,mensaje:mensaje});
				}
			});
		}
	});
};

exports.getMensajes=function(req,res,next){
	Mensajes.find({})
	.exec(function(err,mensajes){
		if(err){
			console.log(err);
		}else{
			res.send({success: true,mensaje:mensajes});
		}
	});
};