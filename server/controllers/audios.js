var Audio = require('../models/audios');
var ObjectId = require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.agregar_comentario = function(req,res,next)
{
	var usuario = req.body.nombreUsuario;
	Audio.findOneAndUpdate({nombreArchivo:req.body.archivo},
	{$push:
		{comentarios:{
			$each: [{nombreEspecialista:req.body.especialista,texto:req.body.texto,fecha:Date()}]
		}

	}},{safe:true,upsert:true},
	function(error,coment)
	{
		if(error)
		{
			console.log(error);
			res.send({success:false});
		}
		else
		{
			res.send({success:true,comentario:{nombreEspecialista:req.body.especialista,texto:req.body.texto,fecha:Date()}});
		}
	});
}

exports.agregar_calificacion = function(req,res,next)
{
	var usuario = req.body.nombreUsuario;
	Audio.findOneAndUpdate({nombreArchivo:req.body.archivo},
	{$push:
		{calificacion:{
			$each: [{nombreEspecialista:req.body.especialista,valoracion:req.body.valoracion,fecha:Date()}]
		}

	}},{safe:true,upsert:true},
	function(error,calif)
	{
		if(error)
		{
			console.log(error);
			res.send({success:false});
		}
		else
		{
			res.send({success:true,calificacion:{nombreEspecialista:req.body.especialista,valoracion:req.body.valoracion,fecha:Date()}});
		}
	});
}

exports.get_audios_useract = function(req,res,next)
{
	var usuario = req.body.nombreUsuario;
	var actividad = req.body.nombreActividad;
	Audio.find({nombreUsuario:usuario,nombreActividad:actividad})
	.exec(function(error,audiosUsuario)
	{
		console.log(audiosUsuario);
		if( error )
		{
			console.log(error);
			res.send({success:false,message:error});
		}
		else
		{
			if( audiosUsuario != null )
			{
				res.send({success:true,audios:audiosUsuario});
			}
			else
			{
				res.send({success:false,audios:null});
			}
		}
	});
}

exports.guardar_audio=function(req,res,next){
	var audio = new Audio({
		nombreUsuario:req.body.nombreUsuario,
		nombreArchivo:req.body.nombreArchivo,
		nombreActividad:req.body.nombreActividad
	});
	audio.save(function(err,audios){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, audio:audios});
		}
	});
};