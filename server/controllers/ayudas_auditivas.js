var Ayudas= require('../models/ayudas');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_ayuda_auditiva=function(req,res,next){
	var ayudas = new Ayudas({
		nombre:req.body.nombreAyuda_auditiva,
	});
	ayudas.save(function(err,ayuda){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true, ayuda_auditiva:ayuda});
		}
	});
};

exports.eliminar_ayuda_auditiva=function(req,res,next){
	var ayuda=req.body._id;
	var query_d=Ayudas.findOne({_id:ayuda});
	query_d.exec(function(err,doc){
		Ayudas.remove({_id:ayuda},function(err, ayuda) {
			if(err){
				console.log(err);
				res.send({success:false, message:err});
			}else{
				res.send({success: true});
			}
    	});
	})
}

exports.getayudas_auditivas=function(req,res,next){
	Ayudas.find({})
	.exec(function(err,ayudas){
		if(err){
			console.log(err);
			res.send({success:false, message:err});
		}else{
			res.send({success: true,ayuda_auditiva:ayudas});
		}
	});
};