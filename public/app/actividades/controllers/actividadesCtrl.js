angular.module('MainApp').controller('actividadesCtrl', function ($scope, $stateParams, $filter, $rootScope, toastF, Session, EnlacesService, EtiquetasService, EvaluacionesService, HabilidadesService, CompetenciasService, AuditoriaService, $ngConfirm) {

    var currentUsername = '';
    var tituloEnlaceAuditoria = '';
    var nombreEvaluacionAuditoria = '';
    var checkeados = [];
    $scope.enlaces_materia = [];
    $scope.etiquetas = [];
    $scope.evaluaciones = [];
    $scope.habilidades = [];
    $scope.competencias = [];
    $scope.divAdministrador = [];

    $scope.item_master_enlaces = {tituloEnlace: "", comentarioEnlace: "", enlace: ""};
    $scope.item_master_etiquetas = {nombreEtiqueta: "", comentarioEtiqueta: ""};
    $scope.item_master_evaluaciones = {nombreEvaluacion: ""};
    $scope.date = new Date();

// Enlaces Activiades

    Session.getUsuario()
            .then(function (usuario)
            {
                currentUser = usuario.data.user.user;
                currentUsername = currentUser.nombre;
                if (angular.equals(currentUser.perfil, "ADMIN"))
                {
                    $scope.divAdministrador = true;
                } else
                {
                    $scope.divAdministrador = false;
                }
            }
            );

    HabilidadesService.getHabilidades()
            .then(function (response)
            {
                if (response.data.success)
                {
                    $scope.habilidades = response.data.habilidad;
                }
            });

    CompetenciasService.getCompetencias()
            .then(function (response)
            {
                if (response.data.success)
                {
                    $scope.competencias = response.data.competencia;
                }
            });

    $scope.guardar_enlace = function () {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Agregar actividad del software',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        tituloEnlaceAuditoria = $scope.enlace.tituloEnlace;
                        EnlacesService.guardarEnlaces({
                            tituloEnlace: $scope.enlace.tituloEnlace,
                            comentarioEnlace: $scope.enlace.comentarioEnlace,
                            enlace: $scope.enlace.enlace,
                            habilidadEnlace: $scope.enlace.habilidadEnlace,
                            competenciaEnlace: $scope.enlace.competenciaEnlace,
                            tipoEvaluacionEnlace: $scope.enlace.evaluacionEnlace,
                            nivelEnlace: $scope.enlace.nivelEnlace,
                            puntosEnlace: $scope.enlace.puntosEnlace
                        }).then(function (response) {
                            if (response.data.success) {
                                toastF.success('Enlace creado con exito!');
                                $scope.enlaces_materia.push(response.data.enlace);
                                angular.copy($scope.item_master_enlaces, $scope.enlace);
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Actividades",
                                    cambio: "Nuevo",
                                    campo: "Todos",
                                    valor: tituloEnlaceAuditoria,
                                    cliente: "Sistema",
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    $scope.eliminar_enlace = function (enlace) {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Eliminar enlace',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        EnlacesService.eliminarEnlace(enlace).then(function (response) {
                            if (response.data.success) {
                                toastF.warning('Enlace eliminada correctamente!');
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Actividades",
                                    cambio: "Eliminar",
                                    campo: "Todos",
                                    valor: enlace.Titulo,
                                    cliente: "Sistema",
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        getEnlaces();
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    EnlacesService.getEnlaces()
            .then(function (response) {
                if (response.data.success) {
                    $scope.enlaces_materia = response.data.enlace;
                }
            });

    getEnlaces = function () {
        EnlacesService.getEnlaces()
                .then(function (response) {
                    if (response.data.success) {
                        $scope.enlaces_materia = response.data.enlace;
                    }
                });
    }

// Etiquetas Activiades

    $scope.guardar_etiqueta = function () {
        EtiquetasService.guardarEtiquetas({
            nombreEtiqueta: $scope.etiqueta.nombreEtiqueta,
            comentarioEtiqueta: $scope.etiqueta.comentarioEtiqueta})
                .then(function (response) {
                    if (response.data.success) {
                        toastF.success('Etiqueta creado con exito!');
                        $scope.etiquetas.push(response.data.enlace);
                        angular.copy($scope.item_master_etiquetas, $scope.etiqueta);
                    }
                });
        getEtiquetas();
    }

    $scope.eliminar_etiqueta = function (etiqueta) {
        EtiquetasService.eliminarEtiqueta(etiqueta)
                .then(function (response) {
                    if (response.data.success) {
                        toastF.warning('Etiqueta eliminada correctamente!');
                    }
                });
        getEtiquetas();
    }

    EtiquetasService.getEtiquetas()
            .then(function (response) {
                if (response.data.success) {
                    $scope.etiquetas = response.data.etiqueta;
                }
            });

    getEtiquetas = function () {
        EtiquetasService.getEtiquetas()
                .then(function (response) {
                    if (response.data.success) {
                        $scope.etiquetas = response.data.etiqueta;
                    }
                });
    }

// Tipo evaluacion

    $scope.guardar_evaluacion = function () {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Agregar nuevo tipo de evaluación',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        nombreEvaluacionAuditoria = $scope.evaluacion.nombreEvaluacion;
                        EvaluacionesService.guardarEvaluaciones({
                            nombreEvaluacion: $scope.evaluacion.nombreEvaluacion}).then(function (response) {
                            if (response.data.success) {
                                toastF.success('Evaluacion creado con exito!');
                                $scope.evaluaciones.push(response.data.enlace);
                                angular.copy($scope.item_master_evaluaciones, $scope.evaluacion);
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Evaluaciones",
                                    cambio: "Nuevo",
                                    campo: "Todos",
                                    valor: nombreEvaluacionAuditoria,
                                    cliente: "Sistema",
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        getEvaluaciones();
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });

    }

    $scope.eliminar_evaluacion = function (evaluacion) {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Eliminar evaluación',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        EvaluacionesService.eliminarEvaluacion(evaluacion).then(function (response) {
                            if (response.data.success) {
                                toastF.warning('Evaluacion eliminada correctamente!');
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Evaluaciones",
                                    cambio: "Eliminar",
                                    campo: "Todos",
                                    valor: evaluacion.Nombre,
                                    cliente: "Sistema",
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        getEvaluaciones();
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    EvaluacionesService.getEvaluaciones()
            .then(function (response) {
                if (response.data.success) {
                    $scope.evaluaciones = response.data.evaluacion;
                }
            });

    getEvaluaciones = function () {
        EvaluacionesService.getEvaluaciones()
                .then(function (response) {
                    if (response.data.success) {
                        $scope.evaluaciones = response.data.evaluacion;
                    }
                });
    }
});