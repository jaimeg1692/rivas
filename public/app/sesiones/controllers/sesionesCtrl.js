angular.module('MainApp').controller('sesionesCtrl',function($scope,$stateParams,$filter,$rootScope,SesionesService,UsuariosService,Session){
    
    var checkeados=[];
	$scope.sesiones=[];
	$scope.sesionesfinal=[];
	var currentUser = null;
	var i=0;
	UsuariosService.get_usuarios({})
	.then(function(usuario){
		if(usuario.data.success)
		{	
			Session.getUsuario()
			.then(function(usuarioActual)
			{	 
				currentUser = usuarioActual.data.user.user;
				if(angular.equals(currentUser.perfil,"ADMIN") || angular.equals(currentUser.perfil,"ESPE"))
					{
						$rootScope.administrador = true;
						SesionesService.getSesiones()
							.then(function(sesiones){
							angular.forEach(usuario.data.usuario, function (usuarios, index) {							
								$scope.listUsuarios=usuario.data.usuario;
						    	$scope.sesionesPorUsuarios = $filter('filter')(sesiones.data.sesion,{nombreUsuario:usuarios.nombre_usuario});
						    	usuarios["sesionesUsuario"] = $scope.sesionesPorUsuarios.length;
						    	usuarios["sesionesSoftware"] = $filter('filter')($scope.sesionesPorUsuarios,{plataforma:'software'});
								usuarios["sesionesJuego"] = $filter('filter')($scope.sesionesPorUsuarios,{plataforma:'juego'});
								console.log(usuarios);
						    });
						});
					}
				if(angular.equals(currentUser.perfil,"USER"))
					{
						$rootScope.administrador = false;
						SesionesService.getSesiones()
							.then(function(sesiones){
								$scope.sesionesfinal = $filter('filter')(sesiones.data.sesion,{nombreUsuario:currentUser.nombre_usuario});
								currentUser["sesionesUsuario"] = $scope.sesionesfinal.length;
								$scope.sesionesSoftware = $filter('filter')($scope.sesionesfinal,{plataforma:'software'});
								$scope.sesionesJuego = $filter('filter')($scope.sesionesfinal,{plataforma:'juego'});
								console.log($scope.sesionesJuego);
								$scope.usuario=currentUser;
						});			
					}
			});				
		}
	});
});