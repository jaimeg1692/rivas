var app = angular.module('MainApp',['ui.router', 'ngAnimate', 'toastr','ui.bootstrap','datatables', 'ngResource','angular.filter','tc.chartjs','ngSanitize','ngCsv','cp.ngConfirm']);

app.config(['$stateProvider',"$urlRouterProvider", function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/app/dashboard');

	$stateProvider
		.state('app',{
			url : '/app',
			templateUrl : 'partials/index/templates/index.html',
			controller : 'indexCtrl'
		})
		.state('app.dashboard',{
			url : '/dashboard',
			templateUrl : 'partials/dashboard/templates/dashboard.html',
			controller : 'dashboardCtrl'
		})
		.state('registro',{
			url : '/registro',
			templateUrl : 'partials/sign/templates/registro.html',
			controller : 'registroCtrl'
		})
		.state('app.perfil',{
			url : '/perfil',
			templateUrl : 'partials/perfil/templates/perfil.html',
		})
		.state('info',{
			url : '/informacion',
			templateUrl : 'partials/informacion/templates/info.html',
		})
		.state('login',{
			url : '/login',
			templateUrl : 'partials/sign/templates/login.html',
			controller : 'loginCtrl'
		})
		.state('app.calendario',{
			url:'/calendario',
			templateUrl:'partials/calendario/templates/calendario.html'
		})
		.state('app.actividades',{
			url:'/actividades',
			templateUrl:'partials/actividades/templates/actividades.html',
			controller:'actividadesCtrl'
		})
		.state('app.puntajes',{
			url:'/puntajes',
			templateUrl:'partials/puntajes/templates/puntajes.html',
			controller:'puntajesCtrl'
		})
		.state('app.historias',{
			url:'/historias',
			templateUrl:'partials/historia_medica/templates/historias.html',
			controller:'historiasMedicasCtrl'
		})
		.state('app.competencias',{
			url:'/competencias',
			templateUrl:'partials/competencias/templates/competencias.html',
			controller:'competenciasCtrl'
		})
		.state('app.criterios',{
			url:'/criterios',
			templateUrl:'partials/criterios/templates/criterios.html',
			controller:'criteriosCtrl'
		})
		.state('app.sesiones',{
			url:'/reportes',
			templateUrl:'partials/sesiones/templates/sesiones.html',
			controller:'sesionesCtrl'
		})
		.state('app.usuarios',{
			url:'/usuarios',
			templateUrl:'partials/usuarios/templates/usuarios.html',
			controller:'usuariosCtrl'
		})		
		.state('app.parametros',{
			url:'/parametros',
			templateUrl:'partials/parametros_registro/templates/parametros_registro.html',
			controller:'parametros_registroCtrl'
		})
		.state('app.acudientes',{
			url:'/acudientes',
			templateUrl:'partials/acudientes/templates/acudientes.html',
			controller:'acudientesCtrl'
		})
		.state('app.audios',{
			url:'/audios',
			templateUrl:'partials/audios/templates/audios.html',
			controller:'audiosCtrl'
		})
		.state('app.evaluacion',{
			url:'/evaluacion',
			templateUrl:'partials/evaluacion/templates/evaluacion.html',
			controller:'evaluacionCtrl'
		})
		.state('app.alertas',{
			url:'/alertas',
			templateUrl:'partials/alertas/templates/alertas.html',
			controller:'alertasCtrl'
		})
		.state('app.avances',{
			url:'/avances',
			templateUrl:'partials/avances/templates/avances.html',
			controller:'avancesCtrl'
		})
		.state('app.auditoria',{
			url:'/auditoria',
			templateUrl:'partials/auditoria/templates/auditoria.html',
			controller:'auditoriaCtrl'
		})		
}]);