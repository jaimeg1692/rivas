angular.module('MainApp').controller('acudientesCtrl',function($scope,$rootScope,$filter,$state,Session,HistoriasMedicasService,UsuariosService,AuditoriaService,toastF){
	
	var currentUsername = '';
	var NombreAcudienteAuditoria = "";
	var ident = "";

	$scope.usuario = null;

	$scope.divUsuario = false;

	$scope.idUsuarioRegistrado = null;

	$scope.acudientes = [];

	Session.getUsuario()
		.then(function(usuario)
		{
			currentUser = usuario.data.user.user;
			currentUsername = currentUser.nombre;
			if(angular.equals(currentUser.perfil,"ADMIN"))
			{
				$scope.divAdministrador = true;
			}
			else
			{
				$scope.divAdministrador = false;
			}		
		}
	);	

	$scope.get_usuario=function(){
	HistoriasMedicasService.getUsuario({identificacion:ident})
	.then(function(response){
		if(response.data.success&&response.data.usuario.habilitado&&angular.equals(response.data.usuario.perfil,"USER"))
		{		
			$scope.usuario = response.data.usuario;
			$scope.idUsuarioRegistrado = ident;
			HistoriasMedicasService.getHistoria({identificacion:$scope.idUsuarioRegistrado})
			.then(function(response)
			{
				if( response.data.success )
				{
					toastF.success('El usuario está en el registro y tiene historia médica!')
					get_acudientes();
				}
				else
				{
					toastF.error('El usuario no tiene historia médica!')
				}
			}
			);
		}
		else
		{
			if( response.data.success )
			{
				if( !response.data.usuario.habilitado )
				{
					toastF.error('El usuario no está habilitado!')
				}
				else
				{
					toastF.error('A este usuario no se puede agregar un acudiente!')
				}
			}
			else
			{
				toastF.error('El usuario no está en el resgitro!')
			}
		}

		});
	}

	get_acudientes=function( )
	{
		HistoriasMedicasService.getAcudientes({identificacion:$scope.idUsuarioRegistrado})
		.then(function(response)
			{
				if(response.data.success)
				{
					$scope.acudientes = response.data.acudientes;
					$scope.divUsuario = true;
				}
			});	
	}

	$scope.agregar_acudiente=function( )
	{
		NombreAcudienteAuditoria = $scope.acudiente.NombreAcudiente;
		HistoriasMedicasService.agregarAcudiente(
			{
				identificacion:$scope.idUsuarioRegistrado,
				nombreac:$scope.acudiente.NombreAcudiente,
				telefonoac:$scope.acudiente.TelefonoAcudiente,
				correoac:$scope.acudiente.CorreoAcudiente,
				direccionac:$scope.acudiente.DireccionAcudiente
			})
		.then(function(response){
			if(response.data.success)
			{
				$scope.acudientes.push(response.data.acudiente);
				divUsuario = true;
				// Registro de cambios
				AuditoriaService.guardar_auditoria({
				tabla: "Acudientes", 
				cambio: "Nueva",
				campo: "Todos", 	
				valor: NombreAcudienteAuditoria,
				cliente: ident,
				usuario: currentUsername}).then(function(responseAudit){
 					if(responseAudit.data.success){
 						console.log("Movimiento guardado en auditoria");
 					} else {
 						console.log("Error en el guardado");
 					}
 				});				
			}
		});
		get_acudientes();
	}

	UsuariosService.get_usuarios({})
	.then(function(response){
		if(response.data.success)
		{		 
			$scope.listUsuariosUser = response.data.usuario;
			$scope.listUsuarios = $filter('filter')($scope.listUsuariosUser,{perfil:'USER'});
		}
	});

	$scope.editar_usuario=function(usuario)
	{
	ident = usuario.identificacion;
	HistoriasMedicasService.getUsuario({identificacion:ident})
	.then(function(response){
		if(response.data.success&&response.data.usuario.habilitado&&angular.equals(response.data.usuario.perfil,"USER"))
		{		
			$scope.usuario = response.data.usuario;
			$scope.idUsuarioRegistrado = ident;
			HistoriasMedicasService.getHistoria({identificacion:$scope.idUsuarioRegistrado})
			.then(function(response)
			{
				if( response.data.success )
				{
					toastF.success('El usuario está en el registro y tiene historia médica!')
					get_acudientes();
				}
				else
				{
					toastF.error('El usuario no tiene historia médica!')
				}
			}
			);
		}
		else
		{
			if( response.data.success )
			{
				if( !response.data.usuario.habilitado )
				{
					toastF.error('El usuario no está habilitado!')
				}
				else
				{
					toastF.error('A este usuario no se puede agregar un acudiente!')
				}
			}
			else
			{
				toastF.error('El usuario no está en el resgitro!')
			}
		}

		});
	}

	$scope.regresar_usuario=function()
	{
		$scope.usuario = null;
		$scope.divUsuario = false;
	}

});