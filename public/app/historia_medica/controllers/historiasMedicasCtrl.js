angular.module('MainApp').controller('historiasMedicasCtrl',function($scope,$rootScope,$filter,$state,HistoriasMedicasService,ComentariosHistoriaService,UsuariosService,EducativoService,RehabilitacionService,IdentificacionService,AyudasService,Session,AuditoriaService,toastF){

	$scope.item_master={
		Nombre:"",
		TipoID:"",
		Identificacion:0,
		FechaNacimiento:"",
		Edad:0,
		Sexo:"",
		Telefono:0,
		Celular:0,
		Correo:"",
		Ciudad:"",
		Estrato:0,
		NivelEducativo:"",
		OidoAyuda:"",
		EdadDiagnosticoPA:0,
		TipoAyuda:"",
		FormaUso:"",
		EdadAdaptacion:0
	};

	$scope.item_master_comentarios={
		Profesional:"",
		Texto:"",
		Identificacion:0
	}

	$scope.registrar = true;
	$scope.usuarios = null;
	$scope.historiaC = null;
	$scope.idUsuarioRegistrado = null;
	$scope.divConsulta = false;
	$scope.divRegistro = false;
	$scope.comentarios = [];
	$scope.divUsuario = true;
	$scope.listaTipoID = [];
	$scope.listaTipoAyuda = [];
	$scope.listaNivelEducativo = [];
	$scope.listaTipoRehabilitacion = [];
	$scope.competencias1={};
	$scope.competencias2={};

/*	
	$scope.competenciasLectoEscritas = [ { 'nombre': 'Reconoce las Vocales' },
									     { 'nombre': 'Escribe las vocales' },
									     { 'nombre': 'Reconoce el abecedario' },
									     { 'nombre': 'Escribe el abecedario' },
									     { 'nombre': 'Puede Leer' },
									     { 'nombre': 'Puede Escribir' },
									     { 'nombre': 'Puede Tomar Dictados' },
									     { 'nombre': 'Ninguna' }];

	$scope.competenciasMatematicas = [ { 'nombre': 'Conoce y cuenta los números hasta 10' },
									     { 'nombre': 'Suma' },
									     { 'nombre': 'Resta' },
									     { 'nombre': 'Divide' },
									     { 'nombre': 'Multiplica' },
									     { 'nombre': 'Ninguna' }];
*/

	var currentUser = null;
	var currentUsername = '';
	var CommentariotextoAuditoria = '';
	var nombreClienteAuditoria = '';
	var ident = '';
	Session.getUsuario()
		.then(function(usuario)
		{
			currentUser = usuario.data.user.user;
			currentUsername = currentUser.nombre;
			if(angular.equals(currentUser.perfil,"USER"))
			{
				$scope.idUsuarioRegistrado = currentUser.identificacion;
				HistoriasMedicasService.getHistoria({identificacion:$scope.idUsuarioRegistrado})
				.then(function(response)
				{
					if( response.data.success )
					{
						$scope.historiaC = response.data.historiaMedica;
						$scope.divConsulta = true;
						$scope.divRegistro = false;
					}
					else
					{
						toastF.error('El usuario no tiene una historia médica diligenciada!')
						$scope.divUsuario = false;
						$scope.divRegistro = true;
						$scope.divConsulta = false;
					}
				}
				);
				ComentariosHistoriaService.getComentarios({identificacion:$scope.idUsuarioRegistrado})
				.then(function(response){
					if( response.data.success )
					{
						$scope.comentarios = response.data.comentarios;
					}
					});
				$scope.divUsuario = false;
				$scope.divRegistro = false;
			}
			else
			{
				$scope.divUsuario = true;
			}		
		}
	);

	EducativoService.getnivel_educativo()
	.then(function(response)
		{
			if( response.data.success )
			{
				$scope.listaNivelEducativo = response.data.nivel_educativo;
			}
			else
			{
				toastF.error('¡No se pudo cargar la lista de niveles educativos!')
			}

		}
	);

	IdentificacionService.getIdentificacion()
	.then(function(response)
		{
			if( response.data.success )
			{
				$scope.listaTipoID  = response.data.identificacion;
				console.log($scope.listaTipoID);
			}
			else
			{
				toastF.error('¡No se pudo cargar la lista de tipos de identificación!')
			}

		}
	);

	AyudasService.getayudas_auditivas()
	.then(function(response)
		{
			if( response.data.success )
			{
				$scope.listaTipoAyuda  = response.data.ayuda_auditiva;
			}
			else
			{
				toastF.error('¡No se pudo cargar la lista de tipos de ayuda auditiva!')
			}

		}
	);

	RehabilitacionService.getrehabilitacion()
	.then(function(response)
		{
			if( response.data.success )
			{
				$scope.listaTipoRehabilitacion  = response.data.rehabilitacion;
			}
			else
			{
				toastF.error('¡No se pudo cargar la lista de tipos de rehabilitacion!')
			}

		}
	);

	$scope.guardar_historia=function(){
		nombreClienteAuditoria = $scope.historia.nombre;
		HistoriasMedicasService.guardarHistoria(
			{	nombre:$scope.historia.nombre,
				tipoid:$scope.historia.tipoid, 
				identi:$scope.historia.identi,
				fechanacimiento:$scope.historia.fechanacimiento,
				edad:$scope.historia.edad,
				sexo:$scope.historia.sexo,
				telefono:$scope.historia.telefono,
				celular:$scope.historia.celular,
				correo:$scope.historia.correo,
				ciudad:$scope.historia.ciudad,
				estrato:$scope.historia.estrato,
				niveleducativo:$scope.historia.niveleducativo, 
				oidoayuda:$scope.historia.oidoayuda,
				edaddiagnosticopa:$scope.historia.edaddiagnosticopa,
				tipoayuda:$scope.historia.tipoayuda,
				formauso:$scope.historia.formauso,
				edadadaptacion:$scope.historia.edadadaptacion,
				tiporehab: $scope.historia.tiporehab,
				diagnosticoasociado: $scope.historia.diagnosticoasociado,
				codigolinguistico: $scope.historia.codigolinguistico, 
				fechacirugia: $scope.historia.fechacirugia,
				reimplante: $scope.historia.reimplante,
				fechaconexion: $scope.historia.fechaconexion,
				sesiones: $scope.historia.sesiones, 
				tiemporehab: $scope.historia.tiemporehab, 
				interrupciones: $scope.historia.interrupciones,
				lenguajecomprensivo1: $scope.historia.lenguajecomprensivo1,
				lenguajecomprensivo2: $scope.historia.lenguajecomprensivo2,
				lenguajeexpresivo1: $scope.historia.lenguajeexpresivo1,
				lenguajeexpresivo2: $scope.historia.lenguajeexpresivo2,
				lenguajeexpresivo3: $scope.historia.lenguajeexpresivo3, 
				seguimiento: $scope.historia.seguimiento,
				fechaprogramacion: $scope.historia.fechaprogramacion,
				fechaaudiometria: $scope.historia.fechaaudiometria,
				tipoinsteducativa: $scope.historia.tipoinsteducativa,
				nombreinsteducativa: $scope.historia.nombreinsteducativa, 
				competencial1: $scope.historia.competencial1,
				competencial2: $scope.historia.competencial2,
				competencial3: $scope.historia.competencial3,
				competencial4: $scope.historia.competencial4,
				competencial5: $scope.historia.competencial5,
				competencial6: $scope.historia.competencial6,
				competencial7: $scope.historia.competencial7,
				competencial8: $scope.historia.competencial8,
				competenciam1: $scope.historia.competenciam1,   
				competenciam2: $scope.historia.competenciam2, 
				competenciam3: $scope.historia.competenciam3,
				competenciam4: $scope.historia.competenciam4,
				competenciam5: $scope.historia.competenciam5,
				competenciam6: $scope.historia.competenciam6,
				Acudientes:$scope.historia.acudientes, 
			}).then(function(response){ 
				if(response.data.success);
				{
					$scope.historiaC = response.data.historia;
					angular.copy($scope.item_master,$scope.historia);
					divConsulta = true;
					divRegistro = false;
						// Registro de cambios
						AuditoriaService.guardar_auditoria({
						tabla: "Historias Medicas", 
						cambio: "Nueva",
						campo: "Todos", 	
						valor: nombreClienteAuditoria,
						cliente: ident,
						usuario: currentUsername}).then(function(responseAudit){
	 						if(responseAudit.data.success){
	 							console.log("Movimiento guardado en auditoria");
	 						} else {
	 							console.log("Error en el guardado");
	 						}
	 					});					 
				}
		});
	}

	$scope.get_usuario=function(){
	ident = $scope.identificacion;
	HistoriasMedicasService.getUsuario({identificacion:ident})
	.then(function(response){
		if(response.data.success&&response.data.usuario.habilitado&&angular.equals(response.data.usuario.perfil,"USER"))
		{		
			$scope.usuario = response.data.usuario;
			$scope.idUsuarioRegistrado = ident;
			toastF.success('El usuario está en el registro!')
			consultar_historia();
		}
		else
		{
			if( response.data.success )
			{
				if( !response.data.usuario.habilitado )
				{
					toastF.error('El usuario no está habilitado!')
				}
				else
				{
					toastF.error('A este usuario no se puede crear una historia clínica!')
				}
			}
			else
			{
				toastF.error('El usuario no está en el resgitro!')
			}
		}

		});
	}

	consultar_historia=function()
	{
		console.log($scope.idUsuarioRegistrado);
		HistoriasMedicasService.getHistoria({identificacion:$scope.idUsuarioRegistrado})
		.then(function(response)
		{
			if( response.data.success )
			{
				$scope.historiaC = response.data.historiaMedica;
				$scope.divConsulta = true;
				get_comentarios();
			}
			else
			{
				$scope.divRegistro = true;
			}
		}
		);
	}

	get_comentarios=function()
	{
		ComentariosHistoriaService.getComentarios({identificacion:$scope.idUsuarioRegistrado})
		.then(function(response){
			if( response.data.success )
			{
				$scope.comentarios = response.data.comentarios;
			}
			});
	}

    $scope.guardar_comentario=function( )
	{	
		CommentariotextoAuditoria = $scope.comentario.Texto;
		Session.getUsuario()
		.then(function(usuario)
		{
			currentUser = usuario.data.user.user;
			currentUsername = currentUser.nombre;
			var user = currentUser;
			ComentariosHistoriaService.guardarComentario({
			profesional:currentUser.nombre, 
			texto:$scope.comentario.Texto,
			identificacion:$scope.idUsuarioRegistrado})
			.then(function(response){
			if(response.data.success){
				$scope.comentarios.push(response.data.comentario);
				angular.copy($scope.item_master_comentarios,comentario);
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Historias Medicas", 
					cambio: "Nueva",
					campo: "Comentario", 	
					valor: CommentariotextoAuditoria,
					cliente: ident,
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});	
			}
		});			
		}
		);
		get_comentarios();
	}

	UsuariosService.get_usuarios({})
	.then(function(response){
		if(response.data.success)
		{		 
			$scope.listUsuariosUser = response.data.usuario;
			$scope.listUsuarios = $filter('filter')($scope.listUsuariosUser,{perfil:'USER'});
		}
	});

	$scope.editar_usuario=function(usuario)
	{
		ident = usuario.identificacion;
		HistoriasMedicasService.getHistoria({identificacion:usuario.identificacion})
		.then(function(response)
		{
			if( response.data.success )
			{
				$scope.usuario = usuario;
				$scope.historiaC = response.data.historiaMedica;
				$scope.divConsulta = true;
				get_comentarios();
			}
			else
			{
				$scope.usuario = usuario;
				$scope.divRegistro = true;
			}
		}
		);
	}

	$scope.regresar_usuario=function()
	{
		$scope.usuario = null;
		$scope.divConsulta = false;
		$scope.divRegistro = false;
	}

	$scope.competenciasSelected1 = function () {
	    $scope.competenciasSeleccionadas1 = $filter('filter')($scope.competenciasLectoEscritas, {checked: true});

	    $scope.competencias1 ={
	    	reconoce_las_vocales:$scope.competenciasLectoEscritas[0].checked,
	    	escribe_las_vocales:$scope.competenciasLectoEscritas[1].checked,
	    	reconoce_el_abecedario:$scope.competenciasLectoEscritas[2].checked,
	    	escribe_el_abecedario:$scope.competenciasLectoEscritas[3].checked,
	    	puede_leer:$scope.competenciasLectoEscritas[4].checked,
	    	puede_escribir:$scope.competenciasLectoEscritas[5].checked, 
	    	puede_tomar_dictados:$scope.competenciasLectoEscritas[6].checked,
	    	ninguna:$scope.competenciasLectoEscritas[7].checked, 
	    };
	    //console.log($scope.competencias1);
	}

	$scope.competenciasSelected2 = function () {
	    $scope.competenciasSeleccionadas2 = $filter('filter')($scope.competenciasMatematicas, {checked: true});
	    
	    $scope.competencias2 ={
	    	conoce_hasta_10:$scope.competenciasMatematicas[0].checked,
	    	suma:$scope.competenciasMatematicas[1].checked,
	    	resta:$scope.competenciasMatematicas[2].checked,
	    	divide:$scope.competenciasMatematicas[3].checked, 
	    	multiplica:$scope.competenciasMatematicas[4].checked,
	    	ninguna:$scope.competenciasMatematicas[5].checked, 
	    };  
	    //console.log($scope.competencias2); 
	}

}); 