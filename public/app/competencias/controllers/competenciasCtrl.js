angular.module('MainApp').controller('competenciasCtrl', function ($scope, toastF, CompetenciasService, Session, HabilidadesService, AuditoriaService, $ngConfirm) {
    var currentUsername = '';
    var nombreHabilidadAuditoria = '';
    var nombreCompetenciaAuditoria = '';
    $scope.competencias = [];
    $scope.habilidades = [];
    $scope.date = new Date();
    $scope.item_master_habilidad = {nombreHabilidad: "", comentarioHabilidad: ""};
    $scope.item_master_competencia = {nombreCompetencia: "", comentarioCompetencia: ""};

// Habilidades Auditivas

    Session.getUsuario()
            .then(function (usuario)
            {
                currentUser = usuario.data.user.user;
                currentUsername = currentUser.nombre;
                if (angular.equals(currentUser.perfil, "ADMIN"))
                {
                    $scope.divAdministrador = true;
                } else
                {
                    $scope.divAdministrador = false;
                }
            }
            );

    $scope.guardar_habilidad = function () {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Agregar habilidad',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        nombreHabilidadAuditoria = $scope.habilidad.nombreHabilidad;
                        HabilidadesService.guardarHabilidad({
                            nombreHabilidad: $scope.habilidad.nombreHabilidad,
                            comentarioHabilidad: $scope.habilidad.comentarioHabilidad}).then(function (response) {
                            if (response.data.success) {
                                toastF.success('Habilidad Auditiva creada con exito!');
                                $scope.habilidades.push(response.data.habilidad);
                                angular.copy($scope.item_master_habilidad, $scope.habilidad);
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Habilidades",
                                    cambio: "Nueva",
                                    campo: "Todos",
                                    valor: nombreHabilidadAuditoria,
                                    cliente: "Sistema",
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        getHabilidades();
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    $scope.eliminar_habilidad = function (habilidad) {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Eliminar habilidad',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        HabilidadesService.eliminarHabilidad(habilidad).then(function (response) {
                            if (response.data.success) {
                                toastF.warning('Habilidad Auditiva eliminada correctamente!');
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Habilidades",
                                    cambio: "Eliminar",
                                    campo: "Todos",
                                    valor: habilidad.Nombre,
                                    cliente: "Sistema",
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        getHabilidades();
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    HabilidadesService.getHabilidades()
            .then(function (response) {
                if (response.data.success) {
                    $scope.habilidades = response.data.habilidad;
                }
            });

    getHabilidades = function () {
        HabilidadesService.getHabilidades()
                .then(function (response) {
                    if (response.data.success) {
                        $scope.habilidades = response.data.habilidad;
                    }
                });
    }

// // Competencias 

    $scope.guardar_competencia = function () {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Agregar competencia auditiva',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        nombreCompetenciaAuditoria = $scope.competencia.nombreCompetencia;
                        CompetenciasService.guardarCompetencia({
                            nombreCompetencia: $scope.competencia.nombreCompetencia,
                            comentarioCompetencia: $scope.competencia.comentarioCompetencia}).then(function (response) {
                            if (response.data.success) {
                                toastF.success('Competencia creada con exito!');
                                $scope.competencias.push(response.data.competencia);
                                angular.copy($scope.item_master_competencia, $scope.competencia);
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Competencias",
                                    cambio: "Nuevo",
                                    campo: "Todos",
                                    valor: nombreCompetenciaAuditoria,
                                    cliente: "Sistema",
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        getCompetencias();
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    $scope.eliminar_competencia = function (competencia) {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Eliminar competencia auditiva',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        CompetenciasService.eliminarCompetencia(competencia).then(function (response) {
                            if (response.data.success) {
                                toastF.warning('Competencia eliminada correctamente!');
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Competencias",
                                    cambio: "Eliminar",
                                    campo: "Todos",
                                    valor: competencia.Nombre,
                                    cliente: "Sistema",
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        getCompetencias();
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    CompetenciasService.getCompetencias()
            .then(function (response) {
                if (response.data.success) {
                    $scope.competencias = response.data.competencia;
                }
            });

    getCompetencias = function () {
        CompetenciasService.getCompetencias()
                .then(function (response) {
                    if (response.data.success) {
                        $scope.competencias = response.data.competencia;
                    }
                });
    }
});