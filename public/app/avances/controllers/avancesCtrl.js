angular.module('MainApp').controller('avancesCtrl', function ($scope, $rootScope, $filter, $state, Session, UsuariosService, AvancesService, HabilidadesService, EnlacesService, AuditoriaService, toastF, $ngConfirm) {

    var currentUsername = '';
    var calificacionAuditoria = '';
    var ComentariotextoAuditoria = '';
    var ident = "";

    $scope.usuario = null;
    $scope.especialista = null;
    $scope.habilidad = null;
    $scope.divBuscarUsuario = true;
    $scope.divBuscarHabilidad = false;
    $scope.divResultado = false;
    $scope.divComentarios = true;
    $scope.divEspecialista = true;
    $scope.listaUsuariosHabilitados = [];
    $scope.listaHabilidades = [];
    $scope.listaActividades = [];
    $scope.listaActPorHab = [];
    $scope.listaAvances = [];
    $scope.listaActividadesChunk = [];
    $scope.listaDatos = [];
    $scope.identificacion = 0;
    $scope.actividadSeleccionada = null;

    Session.getUsuario()
            .then(function (usuario)
            {
                currentUser = usuario.data.user.user;
                currentUsername = currentUser.nombre;
                if (angular.equals(currentUser.perfil, "ESPE") || angular.equals(currentUser.perfil, "ADMIN"))
                {
                    $scope.especialista = currentUser;
                    UsuariosService.getUsuariosHabilitados({})
                            .then(function (response)
                            {
                                if (response.data.success)
                                {
                                    $scope.listaUsuariosHabilitados = response.data.usuariosHabilitados;
                                } else
                                {
                                    toastF.error('¡No hay usuarios habilitados en el sistema!')
                                }
                            }
                            );
                } else
                {
                    $scope.especialista = currentUser;
                    $scope.seleccionar_usuario(currentUser);
                    $scope.divBuscarUsuario = false;
                    $scope.divBuscarHabilidad = true;
                    $scope.divEspecialista = false;
                }
            }
            );

    HabilidadesService.getHabilidades()
            .then(function (response)
            {
                if (response.data.success)
                {
                    $scope.listaHabilidades = response.data.habilidad;
                    EnlacesService.getEnlaces()
                            .then(function (response)
                            {
                                if (response.data.success)
                                {
                                    $scope.listaActividades = response.data.enlace;
                                    for (var i = 0; i < $scope.listaHabilidades.length; i++)
                                    {
                                        var H = $scope.listaHabilidades[i];
                                        var nH = H.Nombre;
                                        var act = [];
                                        for (var j = 0; j < $scope.listaActividades.length; j++)
                                        {
                                            if (angular.equals($scope.listaActividades[j].Habilidad.Nombre, nH))
                                            {
                                                act.push($scope.listaActividades[j]);
                                            }
                                        }
                                        ;
                                        act = $filter('orderBy')(act, 'Nivel');
                                        var conjunto = {Habilidad: nH, Actividades: act};
                                        $scope.listaActPorHab.push(conjunto);
                                    }
                                    ;
                                }
                            });
                }
            });

    $scope.seleccionar_usuario = function (user)
    {
        $scope.usuario = user;
        ident = user.identificacion;
        console.log(ident + "; usuario = " + currentUsername);
        $scope.divBuscarUsuario = false;
        $scope.divBuscarHabilidad = true;
        if ($scope.divEspecialista)
        {
            for (var i = 0; i < $scope.listaActPorHab.length; i++)
            {
                var act = $scope.listaActPorHab[i].Actividades;
                for (var j = 0; j < act.length; j++)
                {
                    var a = act[j];
                    AvancesService.crearAvance({Usuario: $scope.usuario, Actividad: a})
                            .then(function (response)
                            {

                            });
                }
                ;
            }
            ;
        }
    }

    $scope.get_usuario = function () {
        ident = $scope.identificacion;
        //console.log(ident+"; usuario = "+currentUsername);
        UsuariosService.getUsuario({identificacion: ident})
                .then(function (response) {
                    if (response.data.success)
                    {
                        $scope.usuario = response.data.usuario;
                        $scope.divBuscarUsuario = false;
                        $scope.divBuscarHabilidad = true;
                        toastF.success('El usuario está en el registro!')
                        for (var i = 0; i < $scope.listaActPorHab.length; i++)
                        {
                            var act = $scope.listaActPorHab[i].Actividades;
                            for (var j = 0; j < act.length; j++)
                            {
                                var a = act[j];
                                AvancesService.crearAvance({Usuario: $scope.usuario, Actividad: a})
                                        .then(function (response)
                                        {

                                        });
                            }
                            ;
                        }
                        ;
                    } else
                    {
                        toastF.error('El usuario no está en el registro!')
                    }
                });
    }

    $scope.regresar_usuario = function ()
    {
        $scope.usuario = null;
        $scope.divBuscarUsuario = true;
        $scope.divBuscarHabilidad = false;
        $scope.listaAvaPorHab = null;
    }

    $scope.seleccionar_habilidad = function (habilidad)
    {
        $scope.habilidad = habilidad;
        for (var i = 0; i < $scope.listaActPorHab.length; i++)
        {
            var conjunto = $scope.listaActPorHab[i];
            if (angular.equals(conjunto.Habilidad, habilidad.Nombre))
            {
                $scope.listaActividadesChunck = conjunto.Actividades;
            }
        }
        ;
        $scope.divBuscarHabilidad = false;
        $scope.divResultado = true;
        get_avances();
    }

    $scope.regresar_habilidad = function (habilidad)
    {
        $scope.habilidad = true;
        $scope.divResultado = false;
        $scope.divBuscarHabilidad = true;
    }

    $scope.calificar_avance = function () {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Agregar calificación: <strong>{{calificacion}}</strong>',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        calificacionAuditoria = $scope.calificacion;
                        AvancesService.agregarCalificacion({
                            nombreUsuario: $scope.usuario.nombre,
                            nombreActividad: $scope.actividadSeleccionada.Titulo,
                            especialista: $scope.especialista.nombre,
                            valoracion: $scope.calificacion
                        }).then(function (response) {
                            if (response.data.success) {
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Avances",
                                    cambio: "Nueva",
                                    campo: $scope.actividadSeleccionada.Titulo + " Calificación",
                                    valor: calificacionAuditoria,
                                    cliente: ident,
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });

                                if ($scope.calificacion == 100) {
                                    AvancesService.completarActividad({nombreUsuario: $scope.usuario.nombre, nombreActividad: $scope.actividadSeleccionada.Titulo}).then(function (response) {
                                        if (response.data.success) {
                                            habilitarSiguienteActividad($scope.actividadSeleccionada);
                                        }
                                    });
                                } else {
                                    get_avances();
                                }
                            }
                        });
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });

    }

    function habilitarSiguienteActividad(actividadActualCompletada)
    {
        var siguienteNivel = actividadActualCompletada.Nivel + 1;
        for (var i = 0; i < $scope.listaAvances.length; i++)
        {
            var act = $scope.listaAvances[i].Actividad;
            if (act.Nivel == siguienteNivel)
            {
                AvancesService.habilitarActividad({nombreUsuario: $scope.usuario.nombre, nombreActividad: act.Titulo})
                        .then(function (response)
                        {

                        });
            }
        }
        ;
        get_avances();
    }

    $scope.comentar_avance = function () {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Agregar comentario',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        ComentariotextoAuditoria = $scope.comentario;
                        AvancesService.agregarComentario({
                            nombreUsuario: $scope.usuario.nombre,
                            nombreActividad: $scope.actividadSeleccionada.Titulo,
                            especialista: $scope.especialista.nombre,
                            texto: $scope.comentario
                        }).then(function (response) {
                            if (response.data.success) {
                                get_avances();
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Avances",
                                    cambio: "Nueva",
                                    campo: $scope.actividadSeleccionada.Titulo + " Comentario",
                                    valor: ComentariotextoAuditoria,
                                    cliente: ident,
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    function get_avances()
    {
        AvancesService.getAvancesUsuario({nombreUsuario: $scope.usuario.nombre, nombreHabilidad: $scope.habilidad.Nombre})
                .then(function (response)
                {
                    if (response.data.success)
                    {
                        $scope.listaAvances = response.data.avances;
                        $scope.listaAvances = $filter('orderBy')($scope.listaAvances, 'Actividad.Nivel');

                        $scope.barrasProgresoValores = [];
                        $scope.barrasProgreso = [];
                        for (var i = 0; i < $scope.listaAvances.length; i++) {
                            arrayProgreso($scope.listaAvances[i].Calificaciones);
                        }
                        ;
                        $scope.listaDatos = [];
                        for (var i = 0; i < $scope.listaAvances.length; i++)
                        {
                            var datos = {Actividad: $scope.listaActividadesChunck[i], Avance: $scope.listaAvances[i], Barras: $scope.barrasProgreso[i]};
                            $scope.listaDatos.push(datos);
                        }
                        ;
                    }
                });
    }


    $scope.seleccionar_actividad = function (actividad)
    {
        $scope.actividadSeleccionada = actividad;
    }

    function arrayProgreso(calificaciones)
    {
        var valores = [];
        calificaciones = $filter('orderBy')(calificaciones, 'valoracion', false);
        for (var i = 0; i < calificaciones.length; i++)
        {
            valores.push(calificaciones[i].valoracion);
        }
        ;
        $scope.barrasProgresoValores.push(valores);
        var dif = [valores[0]];
        for (var i = 0; i < valores.length - 1; i++) {
            dif.push(valores[i + 1] - valores[i]);
        }
        ;
        $scope.barrasProgreso.push(dif);
    }
});