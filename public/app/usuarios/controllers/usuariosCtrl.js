angular.module('MainApp').controller('usuariosCtrl',function($scope,$rootScope,$filter,$state,Session,UsuariosService,AuditoriaService,toastF){

	var currentUsername = '';
	var ident = "";
	var cambioEstadoAuditoria = '';
	var cambioPerfilAuditoria  = '';

	$scope.usuario = null;
	$scope.divUsuario = false;
	$scope.idUsuarioRegistrado = null;


	Session.getUsuario()
		.then(function(usuario)
		{
			currentUser = usuario.data.user.user;
			currentUsername = currentUser.nombre;
			if(angular.equals(currentUser.perfil,"ADMIN"))
			{
				$scope.divAdministrador = true;
			}
			else
			{
				$scope.divAdministrador = false;
			}		
		}
	);	

	UsuariosService.get_usuarios({})
	.then(function(response){
		if(response.data.success)
		{		 
			$scope.listUsuarios = response.data.usuario;
		}
	});

	get_usuarios=function(){
	UsuariosService.get_usuarios({})
	.then(function(response){
		console.log(response.data.success);
		if(response.data.success)
		{		 
			console.log(response.data.usuario);
			$scope.listUsuarios = response.data.usuario;
		}
	});
	}

	$scope.get_usuario=function(){
	ident = $scope.identificacion;
	UsuariosService.getUsuario({identificacion:ident})
	.then(function(response){
		if(response.data.success)
		{		
			$scope.usuario = response.data.usuario;
			$scope.idUsuarioRegistrado = ident;
			toastF.success('El usuario está en el registro!')
			$scope.divUsuario = true;
		}
		else
		{
			toastF.error('El usuario no está en el registro!')
		}

		});
	}

	$scope.actualizar_usuario=function( )
	{
		cambioEstadoAuditoria = $scope.habilitado;
		cambioPerfilAuditoria  = $scope.perfil;
		UsuariosService.actualizarUsuario({identificacion:$scope.idUsuarioRegistrado,habilitado:$scope.habilitado,perfil:$scope.perfil})
		.then(function(response)
			{
				if(response.data.success)
				{
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
						tabla: "Usuarios", 
						cambio: "Actualizar",
						campo: "Estado - Perfil", 	
						valor: "Estado: "+cambioEstadoAuditoria + "; Perfil: " + cambioPerfilAuditoria,
						cliente: ident,
						usuario: currentUsername}).then(function(responseAudit){
					 		if(responseAudit.data.success){
					 			console.log("Movimiento guardado en auditoria");
					 		} else {
					 			console.log("Error en el guardado");
					 		}				 	
				 	});
				 	$scope.divUsuario = false;								
				}
			});	
	}

	$scope.habilitar_usuario=function( )
	{
		UsuariosService.habilitarUsuario({identificacion:$scope.idUsuarioRegistrado,habilitado:$scope.habilitado})
		.then(function(response)
			{
				if(response.data.success)
				{
					UsuariosService.getUsuario({identificacion:$scope.idUsuarioRegistrado})
					.then(function(response){
						if(response.data.success)
						{		
							$scope.usuario = response.data.usuario;
							$scope.idUsuarioRegistrado = ident;
							toastF.success('El usuario está en el registro!')
							$scope.divUsuario = true;
						}
						else
						{
							toastF.error('El usuario no está en el registro!')
						}

						});					
				}
			});	
	}

	$scope.habilitar=function(usuario)
	{
		console.log(usuario);
		ident = usuario.identificacion;
		var estadoAuditoria = !usuario.habilitado;
		UsuariosService.habilitarUsuario({identificacion:usuario.identificacion,habilitado:!usuario.habilitado})
		.then(function(response)
			{
				if(response.data.success)
				{
					toastF.success("El usuario "+usuario.nombre_usuario+" ha cambiado su estado");
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Usuarios", 
					cambio: "Actualizar",
					campo: "Estado", 	
					valor: estadoAuditoria,
					cliente: ident,
					usuario: currentUsername}).then(function(responseAudit){
	 					if(responseAudit.data.success){
	 						console.log("Movimiento guardado en auditoria");
	 					} else {
	 						console.log("Error en el guardado");
	 					}
	 				});						
				}
			});
		get_usuarios();
	}
});