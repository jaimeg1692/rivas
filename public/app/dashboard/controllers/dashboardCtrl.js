angular.module('MainApp').controller('dashboardCtrl', function($scope,$rootScope,Session,$filter,$state,MensajesService,SesionesService){
	$rootScope.administrador = false;
	$rootScope.usuario = true;
	$rootScope.especialista = false;

	$scope.ocultar=true;

	$scope.mensajes=[];
	$scope.mensajesfinal=[];
	var nombreUsuario="";
	var currentUser = null;
	$scope.currentDate= new Date();
	
	Session.getUsuario()
	.then(function(response){
		$scope.usuario = response.data.user.user;
		$rootScope.perfil = $scope.usuario.perfil;
		if (angular.equals($rootScope.perfil,"ADMIN"))
		{
			$rootScope.administrador = true;
			$scope.ocultar=false;
		}
		else if(angular.equals($rootScope.perfil,"ESPE"))
		{
			$rootScope.especialista = true;
			$scope.ocultar=false;
		}
		else
		{
			$rootScope.usuario = true;
			$scope.ocultar=true;
		}	
	});
	
	MensajesService.getMensajes({})
		.then(function(mensaje)
		{
			if(mensaje.data.success){
				Session.getUsuario()
				.then(function(response){
					mensajesUser = mensaje.data.mensaje;
					currentUser= response.data.user.user.nombre_usuario;
					$scope.mensajesfinal = $filter('filter')(mensajesUser,{destinatario:currentUser});
					$scope.mensajes = $scope.mensajesfinal;
					$scope.mensajesfinalSendbox = $filter('filter')(mensajesUser,{remitente:currentUser});
					$scope.mensajesSendbox = $scope.mensajesfinalSendbox;
				});	
			}		
		});

		SesionesService.getSesiones({}).then(function(sesion)
		{
			var contador_dia=0;
			var contador_semana=0;

			if(sesion.data.success){
				$scope.sesionesTotales=sesion.data.sesion.length;
				var fecha=$scope.currentDate.toString();
				var partes=fecha.split(" ");

				if(partes[1]=="Jan"){ partes[1]="01";}
				if(partes[1]=="Feb"){ partes[1]="02";}
				if(partes[1]=="Mar"){ partes[1]="03";}
				if(partes[1]=="Apr"){ partes[1]="04";}
				if(partes[1]=="May"){ partes[1]="05";}
				if(partes[1]=="Jun"){ partes[1]="06";}
				if(partes[1]=="Jul"){ partes[1]="07";}
				if(partes[1]=="Aug"){ partes[1]="08";}
				if(partes[1]=="Sep"){ partes[1]="09";}
				if(partes[1]=="Oct"){ partes[1]="10";}
				if(partes[1]=="Nov"){ partes[1]="11";}
				if(partes[1]=="Dec"){ partes[1]="12";}

				var dias_semana=0;

				if(partes[0]=="Mon"){ dias__semana=0;}
				if(partes[0]=="Tue"){ dias__semana=1;}
				if(partes[0]=="Wed"){ dias__semana=2;}
				if(partes[0]=="Thu"){ dias__semana=3;}
				if(partes[0]=="Fri"){ dias__semana=4;}
				if(partes[0]=="Sat"){ dias__semana=5;}
				if(partes[0]=="Sun"){ dias__semana=6;}

				var date=partes[2]+"/"+partes[1]+"/"+partes[3];

				for(i=0; i<sesion.data.sesion.length; i++){
					var getcurrObj=sesion.data.sesion[i];
					var partes2=getcurrObj.fecha.split("T");
					var partes3=partes2[0].split("-")
					fecha_obj=partes3[2]+"/"+partes3[1]+"/"+partes3[0];
					
					date_obj=new Date(getcurrObj.fecha);
					if(fecha_obj==date){
						contador_dia++;
					}
					//console.log(fecha_obj);
					//console.log(date);
					//console.log(contador_dia);
					var comparacion=new Date();
					comparacion.setDate($scope.currentDate.getDate()-dias__semana-1);
					//if(date_obj>comparacion || date_obj==comparacion){
					if(date_obj>=comparacion){
						contador_semana++; 
					}

				}
				$scope.sesionesDia=contador_dia;
				$scope.sesionesSemana=contador_semana;  
			}		
		});

});