angular.module('MainApp').controller('parametros_registroCtrl',function($scope,$stateParams,$filter,$rootScope,toastF,Session,AyudasService,RehabilitacionService,EducativoService,IdentificacionService,OrganizacionService,AuditoriaService){
	
	var currentUsername = "";
	var nombreAyuda_auditivaAuditoria = "";
	var nombreRehabilitacionAuditoria = "";
	var nombreNivel_educativoAuditoria = "";
	var nombreIdentificacionAuditoria = "";
	var nombreOrganizacionAuditoria = "";

	var checkeados=[];
	$scope.ayudas_auditivas=[];
	$scope.rehabilitaciones=[];
	$scope.niveles_educativos=[];
	$scope.identificaciones=[];
	$scope.organizaciones=[];	

	$scope.item_master_ayuda_auditiva={nombreAyuda_auditiva:""};
	$scope.item_master_rehabilitacion={nombreEtiqueta:""};
	$scope.item_master_nivel_educativo={nombreNivel_educativo:""};
	$scope.item_master_identificacion={nombreIdentificacion:""};
	$scope.item_master_organizacion={nombreOrganizacion:""};
	$scope.date = new Date();

// sesion usuario
	Session.getUsuario()
		.then(function(usuario)
		{
			currentUser = usuario.data.user.user;
			currentUsername = currentUser.nombre;		
		}
	);
// Tipo de ayuda auditiva
	$scope.guardar_ayuda_auditiva=function(){
			nombreAyuda_auditivaAuditoria = $scope.ayuda_auditiva.nombreAyuda_auditiva;
			AyudasService.guardarAyudas({
				nombreAyuda_auditiva:$scope.ayuda_auditiva.nombreAyuda_auditiva,
			})
			.then(function(response){
				if(response.data.success){
					toastF.success('La ayuda auditiva se ha creado con exito!');
					angular.copy($scope.item_master_ayuda_auditiva,$scope.ayuda_auditiva);
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Ayuda auditiva", 
					cambio: "Nueva",
					campo: "Todos", 	
					valor: nombreAyuda_auditivaAuditoria,
					cliente: "Sistema",
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});					
				}
			});
			getayudas_auditivas();
	}

	$scope.eliminar_ayuda_auditiva=function(ayuda_auditiva){
  		AyudasService.eliminarAyudas(ayuda_auditiva)  		
  		.then(function(response){
				if(response.data.success){
					toastF.warning('La ayuda auditiva se ha eliminado correctamente!');
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Ayuda auditiva", 
					cambio: "Eliminar",
					campo: "Todos",
					valor: ayuda_auditiva.nombre,
					cliente: "Sistema", 
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});							
				}
			});
			getayudas_auditivas(); 
	}

	AyudasService.getayudas_auditivas({})
	.then(function(response){
		if(response.data.success){
				$scope.ayudas_auditivas=response.data.ayuda_auditiva;
		}
	});

	getayudas_auditivas = function(){
		AyudasService.getayudas_auditivas({})
		.then(function(response){
			if(response.data.success){
					$scope.ayudas_auditivas=response.data.ayuda_auditiva;
			}
		});
	}

// Tipo de rehabilitacion
	$scope.guardar_rehabilitacion=function(){
			nombreRehabilitacionAuditoria = $scope.rehabilitacion.nombreRehabilitacion;
			RehabilitacionService.guardarRehabilitacion({
				nombreRehabilitacion:$scope.rehabilitacion.nombreRehabilitacion,
			})
			.then(function(response){
				if(response.data.success){
					toastF.success('El tipo de rehabilitación se ha creado con exito!');
					angular.copy($scope.item_master_rehabilitacion,$scope.rehabilitacion);
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Rehabilitacion", 
					cambio: "Nueva",
					campo: "Todos", 	
					valor: nombreRehabilitacionAuditoria,
					cliente: "Sistema",
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});						
				}
			});
			getrehabilitacion();
	}

	$scope.eliminar_rehabilitacion=function(rehabilitacion){
  		RehabilitacionService.eliminarRehabilitacion(rehabilitacion)  		
  		.then(function(response){
				if(response.data.success){
					toastF.warning('El tipo de rehabilitación se ha eliminado correctamente!');
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Rehabilitacion", 
					cambio: "Eliminar",
					campo: "Todos",
					valor: rehabilitacion.nombre,
					cliente: "Sistema", 
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});						
				}
			});
			getrehabilitacion(); 
	}

	RehabilitacionService.getrehabilitacion({})
	.then(function(response){
		if(response.data.success){
			$scope.rehabilitaciones=response.data.rehabilitacion;
		}
	});

	getrehabilitacion = function(){
		RehabilitacionService.getrehabilitacion({})
		.then(function(response){
			if(response.data.success){
					$scope.rehabilitaciones=response.data.rehabilitacion;
			}
		});
	}

// Nivel educativo

	$scope.guardar_nivel_educativo=function(){
			nombreNivel_educativoAuditoria = $scope.nivel_educativo.nombreNivel_educativo;
			EducativoService.guardarEducativo({
				nombreNivel_educativo:$scope.nivel_educativo.nombreNivel_educativo,
			})
			.then(function(response){
				if(response.data.success){
					toastF.success('El nivel educativo se ha creado con exito!');
					angular.copy($scope.item_master_nivel_educativo,$scope.nivel_educativo);
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Nivel educativo", 
					cambio: "Nueva",
					campo: "Todos", 	
					valor: nombreNivel_educativoAuditoria,
					cliente: "Sistema",
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});						
				}
			});
			getnivel_educativo();
	}

	$scope.eliminar_nivel_educativo=function(nivel_educativo){
  		EducativoService.eliminarEducativo(nivel_educativo)  		
  		.then(function(response){
				if(response.data.success){
					toastF.warning('El nivel educativo se ha eliminado correctamente!');
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Nivel educativo", 
					cambio: "Eliminar",
					campo: "Todos",
					valor: nivel_educativo.nombre,
					cliente: "Sistema", 
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});						
				}
			});
			getnivel_educativo(); 
	}

	EducativoService.getnivel_educativo({})
	.then(function(response){
		if(response.data.success){
				$scope.niveles_educativos=response.data.nivel_educativo;
		}
	});

	getnivel_educativo = function(){
		EducativoService.getnivel_educativo({})
		.then(function(response){
			if(response.data.success){
					$scope.niveles_educativos=response.data.nivel_educativo;
			}
		});
	}

// Tipo de identificaciones

	$scope.guardar_identificacion=function(){
			nombreIdentificacionAuditoria = $scope.identificacion.nombreIdentificacion;
			IdentificacionService.guardarIdentificacion({
				nombreNivel_educativo:$scope.identificacion.nombreIdentificacion,
			})
			.then(function(response){
				if(response.data.success){
					toastF.success('El tipo de identificación se ha creado con exito!');
					angular.copy($scope.item_master_identificacion,$scope.identificacion);
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Tipo identificacion", 
					cambio: "Nueva",
					campo: "Todos", 	
					valor: nombreIdentificacionAuditoria,
					cliente: "Sistema",
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});						
				}
			});
			getidentificacion();
	}

	$scope.eliminar_identificacion=function(identificacion){
  		IdentificacionService.eliminarIdentificacion(identificacion)  		
  		.then(function(response){
				if(response.data.success){
					toastF.warning('El tipo de identificación se ha eliminado correctamente!');
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Tipo identificacion", 
					cambio: "Eliminar",
					campo: "Todos",
					valor: identificacion.nombre,
					cliente: "Sistema", 
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});						
				}
			});
			getidentificacion(); 
	}

	IdentificacionService.getIdentificacion({})
	.then(function(response){
		if(response.data.success){
				$scope.identificaciones=response.data.identificacion;
		}
	});

	getidentificacion = function(){
		IdentificacionService.getIdentificacion({})
		.then(function(response){
			if(response.data.success){
					$scope.identificaciones=response.data.identificacion;
			}
		});
	}

// Organizaciones

	$scope.guardar_organizacion=function(){	
			nombreOrganizacionAuditoria = $scope.organizacion.nombreOrganizacion;
			OrganizacionService.guardarOrganizacion({
				nombreOrganizacion:$scope.organizacion.nombreOrganizacion,
			})
			.then(function(response){
				if(response.data.success){
					toastF.success('La organización se ha creado con exito!');
					angular.copy($scope.item_master_organizacion,$scope.organizacion);
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Organizaciones", 
					cambio: "Nueva",
					campo: "Todos", 	
					valor: nombreOrganizacionAuditoria,
					cliente: "Sistema",
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});						
				}
			});
			getOrganizacion();
	}

	$scope.eliminar_organizacion=function(organizacion){
  		OrganizacionService.eliminarOrganizacion(organizacion)  		
  		.then(function(response){
				if(response.data.success){
					toastF.warning('La organización se ha eliminado correctamente!');
					// Registro de cambios
					AuditoriaService.guardar_auditoria({
					tabla: "Organizaciones", 
					cambio: "Eliminar",
					campo: "Todos",
					valor: organizacion.nombre,
					cliente: "Sistema", 
					usuario: currentUsername}).then(function(responseAudit){
 						if(responseAudit.data.success){
 							console.log("Movimiento guardado en auditoria");
 						} else {
 							console.log("Error en el guardado");
 						}
 					});					
				}
			});
			getOrganizacion(); 
	}

	OrganizacionService.getOrganizacion({})
	.then(function(response){
		if(response.data.success){
				$scope.organizaciones=response.data.organizacion;
		}
	});

	getOrganizacion = function(){
		OrganizacionService.getOrganizacion({})
		.then(function(response){
			if(response.data.success){
					$scope.organizaciones=response.data.organizacion;
			}
		});
	}

});

