var app = angular.module('MainApp');

app.controller('indexCtrl', function($rootScope,$state, $scope, $filter, Session,MensajesService,AlertasService,UsuariosService,toastF){

	function Modulo(state){

		this.state = state.name;

		this.name = state.name.split('.')[1];

		this.getName = function(){
			return this.name && this.name[0].toUpperCase() + this.name.slice(1);
		};
	}
	
	
	/////////*********Scopes********//////////
	var role="";
	$scope.modulo = new Modulo($state.current).getName();
	$rootScope.perfil = null;
	$scope.sesionPerfil="";
	$scope.alertas=[];
	$scope.alertasPerfil = false;   
	
	$scope.logout = function(){
		Session.logOut()
		.then(function(response){
			if (response.data.destroy) {
				$state.go('login');
			}
		});
	}

	$scope.habilitar=function(alerta)
	{
		console.log(alerta);
		UsuariosService.habilitarUsuario({identificacion:alerta.doc_usuario,habilitado:true})
		.then(function(response)
			{
				if(response.data.success)
				{
					toastF.success("el usuario "+alerta.usuario+" ha sido habilitado");
					eliminar_alertas(alerta);				
				}
				get_alertas();
			});	
		get_alertas();
	}
	
	Session.isLogged()
	.then(function(response){
		var isLogged = response.data.isLogged;
		if (!isLogged) {
			$state.go('login');
		}
	});
	
	Session.getUsuario()
	.then(function(response){
		$scope.usuario = response.data.user.user;
		if(response.data.user.user.perfil=="ADMIN"){
			$scope.sesionPerfil="Administrador";
		}
		if(response.data.user.user.perfil=="ESPE"){
			$scope.sesionPerfil="Especialista";
		}
		if(response.data.user.user.perfil=="USER"){
			$scope.sesionPerfil="Usuario";
		}
		$rootScope.perfil = $scope.usuario.perfil;
		role=$scope.sesionPerfil;

		get_alertas();
	});


	get_alertas=function(){
		AlertasService.getAlertas({}).then(function(response){
		if(response.data.success){
			if((role=="Administrador")||(role=="Especialista")){					
				//$scope.alertas = response.data.alerta;
				$scope.todasAlertas=response.data.alerta;
				//console.log($scope.todasAlertas);
				$scope.alertas = $filter('filter')($scope.todasAlertas,{tipo_alerta:"REGISTER"});
				//console.log($scope.alertas);
				$scope.numeroAlertas=$scope.alertas.length;
				if ($scope.alertas.length>0) {$scope.alertasPerfil=true;}
				if ($scope.alertas.length==0) {$scope.alertasPerfil=false;}	
			}
		}
		else{

		}
	});
	}

	eliminar_alertas=function(alerta){
  		AlertasService.eliminarAlerta(alerta)  		
  		.then(function(response){
				if(response.data.success){
					toastF.warning('Alerta eliminada correctamente!'); 
				}
				get_alertas(); 
			});
			get_alertas(); 
	}


$scope.item_master_mensaje={destinatario:"",asunto:"",texto:""};

	$scope.guardar_mensaje=function(){
		var nombreUsuario=$scope.usuario.nombre_usuario;
 		
		MensajesService.guardarMensaje({
			remitenteMensaje:nombreUsuario,	
			destinatarioMensaje:$scope.destinatario,
			asuntoMensaje:$scope.asunto,
			mensajeMensaje:$scope.texto
		})
		.then(function(response){
			if(response.data.success){
				toastF.success('El mensaje se ha enviado');
			}
		});
	}

	//Events
	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
		
		$scope.modulo = new Modulo(toState).getName();
	});

});	