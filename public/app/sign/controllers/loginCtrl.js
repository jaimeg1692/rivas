angular.module('MainApp').controller('loginCtrl', function($scope, $http, $state, toastF, Session, SesionesService, IdentificacionService, UsuariosService, AlertasService){
	$scope.master = {};
	var intentos_fallidos=0;
	$scope.usuario = {};

	$scope.listaTipoID = [];

	IdentificacionService.getIdentificacion()
	.then(function(response)
		{
			if( response.data.success ) 
			{
				$scope.listaTipoID  = response.data.identificacion;
			} 
			else
			{
				toastF.error('¡No se pudo cargar la lista de tipos de identificación!')
			}

		}
	);

	$scope.signin = function(){
		var usuario = {username : $scope.usuario.username, password : $scope.usuario.password, fecha: Date()};
		Session.logIn(usuario).then(function (responseRegistered){	
			if (responseRegistered.data.success) {
				UsuariosService.get_usuarioByUserName({nombre_usuario:$scope.usuario.username}).then(function(responseUser){
				//if(responseUser.data.usuario.habilitado){
					SesionesService.guardarSesiones(usuario).then(function (response){
					toastF.success('Iniciaste sesión correctamente!');
					$state.transitionTo('app.dashboard');
					});
				 
				//}else{
				//	toastF.warning('No has sido habilitado para usar el software');
				//}
				});


			}else{
				toastF.error('Error de autenticación, verifica tus datos!');
				intentos_fallidos++;

				if(intentos_fallidos>=5){

				UsuariosService.get_usuarioByUserName({nombre_usuario:$scope.usuario.username}).then(function(responseUser){
				
				var nombre=responseUser.data.usuario.nombre;
				var ident=responseUser.data.usuario.identificacion;
				
					var alerta = {usuario: nombre , doc_usuario : ident, tipo_alerta:"PASSWORD", fecha: Date()};   

					$http.post('/alertas_save', alerta).then(function(alertas){
						if(alertas.data.success){
						toastF.warning('Ha ingresado la contraseña 5 veces mal, se enviara una alerta para la verificacion de la contraseña');
						}else{
						toastF.error('No se pudo guardar la alerta');	
						}
					});
				}); 
				}
			}
		});
	}

	$scope.register = function(){
	$scope.enviando = true;
	$http.post('/registro' , $scope.usuario)
		.then(function(response){
			var data = response.data;
			if (data.success) {
					toastF.error('Ya te encuentras registrado, pero un especialista debe habilitarte para comenzar a usar la herramienta web');
					var alerta = {usuario:$scope.usuario.nombre, doc_usuario:$scope.usuario.identificacion, tipo_alerta:"REGISTER"};
					$http.post('/alertas_save', alerta).then(function(alertas){
						if(alertas.data.success){
						toastF.success('Se envió la alerta para ser habilitado por un especialista');
						}else{
						toastF.error('No se pudo guardar la alerta');	    
						}
					});
					$state.go('login');
			}else{
				$scope.enviando = false;
			};
			if(data.invalid){
				toastF.error('Invalid e-mail');
			}
		});
	}

	Session.isLogged().then(function(response){
		var isLogged = response.data.isLogged;
		if (isLogged && $scope.usuario.habilitado) {
			$state.go('app.dashboard');
		}
	});

});
