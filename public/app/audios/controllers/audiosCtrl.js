angular.module('MainApp').controller('audiosCtrl', function ($scope, $rootScope, $filter, $state, Session, UsuariosService, AudiosService, HabilidadesService, EnlacesService, AuditoriaService, toastF, $ngConfirm) {

    var ident = "";
    var currentUsername = '';
    var calificacionAuditoria = '';
    var nombreActividad = '';
    var textoComentarioAuditoria = '';

    $scope.usuario = null;
    $scope.especialista = null;
    $scope.actividad = null;
    $scope.habilidad = null;
    $scope.divBuscarUsuario = true;
    $scope.divBuscarHabilidad = false;
    $scope.divBuscarActividad = false;
    $scope.divResultado = false;
    $scope.divComentarios = true;
    $scope.divEspecialista = true;
    $scope.listaUsuariosHabilitados = [];
    $scope.listaHabilidades = [];
    $scope.listaActividades = [];
    $scope.listaActPorHab = [];
    $scope.listaAudios = [];
    $scope.listaActividadesChunk = [];
    $scope.identificacion = 0;
    $scope.audioSeleccionado = null;

    Session.getUsuario()
            .then(function (usuario)
            {
                currentUser = usuario.data.user.user;
                currentUsername = currentUser.nombre;
                if (angular.equals(currentUser.perfil, "ESPE") || angular.equals(currentUser.perfil, "ADMIN"))
                {
                    $scope.especialista = currentUser;
                    UsuariosService.getUsuariosHabilitados({})
                            .then(function (response)
                            {
                                if (response.data.success)
                                {
                                    $scope.listaUsuariosHabilitados = response.data.usuariosHabilitados;
                                } else
                                {
                                    toastF.error('¡No hay usuarios habilitados en el sistema!')
                                }
                            }
                            );
                } else
                {
                    $scope.seleccionar_usuario(currentUser);
                    $scope.divBuscarUsuario = false;
                    $scope.divBuscarHabilidad = true;
                    $scope.divEspecialista = false;
                }
            }
            );

    HabilidadesService.getHabilidades()
            .then(function (response)
            {
                if (response.data.success)
                {
                    $scope.listaHabilidades = response.data.habilidad;
                    EnlacesService.getEnlaces()
                            .then(function (response)
                            {
                                if (response.data.success)
                                {
                                    $scope.listaActividades = response.data.enlace;
                                    for (var i = 0; i < $scope.listaHabilidades.length; i++)
                                    {
                                        var H = $scope.listaHabilidades[i];
                                        var nH = H.Nombre;
                                        var act = [];
                                        for (var j = 0; j < $scope.listaActividades.length; j++)
                                        {
                                            if (angular.equals($scope.listaActividades[j].Habilidad.Nombre, nH))
                                            {
                                                act.push($scope.listaActividades[j]);
                                            }
                                        }
                                        ;
                                        var conjunto = {Habilidad: nH, Actividades: act};
                                        $scope.listaActPorHab.push(conjunto);
                                    }
                                    ;
                                }
                            });
                }
            });

    $scope.seleccionar_usuario = function (user)
    {
        $scope.usuario = user;
        ident = user.identificacion;
        $scope.divBuscarUsuario = false;
        $scope.divBuscarHabilidad = true;
    }

    $scope.get_usuario = function () {
        ident = $scope.identificacion;
        UsuariosService.getUsuario({identificacion: ident})
                .then(function (response) {
                    if (response.data.success)
                    {
                        $scope.usuario = response.data.usuario;
                        toastF.success('El usuario está en el registro!')
                        $scope.divBuscarUsuario = false;
                        $scope.divBuscarHabilidad = true;
                    } else
                    {
                        toastF.error('El usuario no está en el registro!')
                    }

                });
    }

    $scope.regresar_usuario = function ()
    {
        $scope.usuario = null;
        $scope.divBuscarUsuario = true;
        $scope.divBuscarHabilidad = false;
    }

    $scope.seleccionar_habilidad = function (habilidad)
    {
        $scope.habilidad = habilidad;
        for (var i = 0; i < $scope.listaActPorHab.length; i++)
        {
            var conjunto = $scope.listaActPorHab[i];
            if (angular.equals(conjunto.Habilidad, habilidad.Nombre))
            {
                $scope.listaActividadesChunck = conjunto.Actividades;
            }
        }
        ;
        $scope.divBuscarHabilidad = false;
        $scope.divBuscarActividad = true;
        $scope.divResultado = false;
    }

    $scope.regresar_habilidad = function (habilidad)
    {
        $scope.habilidad = null;
        $scope.divBuscarHabilidad = true;
        $scope.divBuscarActividad = false;
    }

    $scope.seleccionar_actividad = function (actividad)
    {
        $scope.actividad = actividad;
        var nombreUsuario = $scope.usuario.nombre_usuario;
        nombreActividad = $scope.actividad.Titulo;
        get_audios();
        $scope.divResultado = true;
    }

    function get_audios()
    {
        var nombreUsuario = $scope.usuario.nombre_usuario;
        var nombreActividad = $scope.actividad.Titulo;
        AudiosService.getAudiosUsuarioActividad({nombreUsuario: nombreUsuario, nombreActividad: nombreActividad})
                .then(function (response)
                {
                    if (response.data.success)
                    {
                        $scope.listaAudios = response.data.audios;
                    }
                });
        console.log("Data:" + $scope.listaAudios);
    }

    $scope.guardar_audio = function () {
        var nombreUsuario = $scope.usuario.nombre_usuario;
        var nombreActividad = $scope.actividad.Titulo;
        AudiosService.guardarAudio(
                {
                    nombreUsuario: nombreUsuario,
                    nombreArchivo: nombreUsuario.concat(nombreActividad, Date().toString()),
                    nombreActividad: nombreActividad,
                })
                .then(function (response) {
                    if (response.data.success)
                    {
                        get_audios();
                    } else
                    {
                        toastF.error('No se creo el Audio!')
                    }
                });
    }

    $scope.calificar_audio = function ()
    {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Agregar calificación: <strong>{{calificacion}}</strong>',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        calificacionAuditoria = $scope.calificacion;
                        AudiosService.agregarCalificacion({
                            nombreUsuario: $scope.usuario.nombre_usuario,
                            archivo: $scope.audioSeleccionado.nombreArchivo,
                            especialista: $scope.especialista.nombre,
                            valoracion: $scope.calificacion
                        }).then(function (response) {
                            if (response.data.success) {
                                get_audios();
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Audios",
                                    cambio: "Nueva",
                                    campo: nombreActividad + " Calificación",
                                    valor: calificacionAuditoria,
                                    cliente: ident,
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });
    }

    $scope.comentar_audio = function ()
    {
        $ngConfirm({
            title: '¿Desea continuar?',
            content: 'Agregar comentario: <strong>{{comentario}}</strong>',
            autoClose: 'logoutUser|10000',
            scope: $scope,
            buttons: {
                cancel: function () {},
                logoutUser: {
                    text: 'Continuar',
                    btnClass: 'btn-green',
                    action: function () {
                        textoComentarioAuditoria = $scope.comentario;
                        AudiosService.agregarComentario({
                            nombreUsuario: $scope.usuario.nombre_usuario,
                            archivo: $scope.audioSeleccionado.nombreArchivo,
                            especialista: $scope.especialista.nombre,
                            texto: $scope.comentario
                        }).then(function (response) {
                            if (response.data.success) {
                                get_audios();
                                // Registro de cambios
                                AuditoriaService.guardar_auditoria({
                                    tabla: "Audios",
                                    cambio: "Nueva",
                                    campo: nombreActividad + " Comentario",
                                    valor: textoComentarioAuditoria,
                                    cliente: ident,
                                    usuario: currentUsername}).then(function (responseAudit) {
                                    if (responseAudit.data.success) {
                                        console.log("Movimiento guardado en auditoria");
                                    } else {
                                        console.log("Error en el guardado");
                                    }
                                });
                            }
                        });
                        $ngConfirm('Cambios realizados.');
                    }
                }
            }
        });

    }


    $scope.seleccionar_audio = function (audio)
    {
        $scope.audioSeleccionado = audio;
    }
});