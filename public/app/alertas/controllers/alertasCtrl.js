angular.module('MainApp').controller('alertasCtrl',function($scope,$stateParams,Session,AlertasService,toastF){
    
    $scope.alertas = [];
    $scope.ocultar=true;

    AlertasService.getAlertas({})
    .then(function(response)
    {
      if(response.data.success){      

          var alertas=response.data.alerta;

          angular.forEach(alertas, function(obj){
            if(obj.tipo_alerta=="PASSWORD"){
              obj["tipo_alerta"]="Contraseña Olvidada";
            }
            if(obj.tipo_alerta=="REGISTER"){
              obj["tipo_alerta"]="Nuevo Usuario";
            }
            
          });
        $scope.alertas=alertas;
      }
      
    });

    function get_alertas(){
    AlertasService.getAlertas({})
    .then(function(response)
    {
      if(response.data.success){      

          var alertas=response.data.alerta;

          angular.forEach(alertas, function(obj){
            if(obj.tipo_alerta=="PASSWORD"){
              obj["tipo_alerta"]="Contraseña Olvidada";
            }
            if(obj.tipo_alerta=="REGISTER"){
              obj["tipo_alerta"]="Nuevo Usuario";
            }
            
          });
        $scope.alertas=alertas;
      }
      
    });
    }

      $scope.eliminar_alertas=function(alerta){
      AlertasService.eliminarAlerta(alerta)     
      .then(function(response){
        if(response.data.success){
          toastF.warning('Alerta eliminada correctamente!');
          get_alertas(); 
        }
      });
  }

  Session.getUsuario()
  .then(function(response){
    $scope.usuario = response.data.user.user;
    if((response.data.user.user.perfil=="ADMIN") || (response.data.user.user.perfil=="ESPE")){
      $scope.ocultar=false; 
    }else{
      $scope.ocultar=true;
    }
    
  });

});