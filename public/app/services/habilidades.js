angular.module('MainApp').factory('HabilidadesService',function($http){
	return{
		guardarHabilidad:function(datos){
			return $http.post('/habilidades',datos)
		},
		eliminarHabilidad:function(habilidad){
			return $http.post('/habilidades_eliminar',habilidad)
		},
		getHabilidades:function(){
			return $http.get('/habilidades');
		}
	}
});