angular.module('MainApp').factory('ComentariosHistoriaService',function($http){
	return{
		guardarComentario:function(datos){
			return $http.post('/comentarios_historia',datos);
		},
		getComentarios:function(identificacion){
			return $http.post('/get_comentarios_historia',identificacion);
		},
	}
});