angular.module('MainApp').factory('EvaluacionesService',function($http){
	return{
		guardarEvaluaciones:function(datos){
			return $http.post('/evaluaciones',datos)
		},
		eliminarEvaluacion:function(evaluacion){
			return $http.post('/evaluaciones_eliminar',evaluacion)
		},
		getEvaluaciones:function(){
			return $http.get('/evaluaciones');
		}
	}
});