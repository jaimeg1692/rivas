angular.module('MainApp').factory('AudiosService',function($http){
	return{
		agregarComentario:function(datos){
			return $http.post('/audios_comentarios',datos);
		},
		agregarCalificacion:function(datos){
			return $http.post('/audios_calificacion',datos);
		},
		getAudiosUsuarioActividad:function(datos){
			return $http.post('/audios_useract',datos);
		},
		guardarAudio:function(datos){
			return $http.post('/audios_guardar',datos);
		},
	}
});