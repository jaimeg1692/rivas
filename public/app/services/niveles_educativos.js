angular.module('MainApp').factory('EducativoService',function($http){
	return{
		guardarEducativo:function(datos){
			return $http.post('/educativos',datos)
		},

		eliminarEducativo:function(datos){
			return $http.post('/educativos_eliminar',datos)
		},
		
		getnivel_educativo:function(datos){
			return $http.post('/educativos_consulta',datos);
		},
	}
});