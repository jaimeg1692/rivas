angular.module('MainApp').factory('AvancesService',function($http){
	return{
		agregarComentario:function(datos){
			return $http.post('/avances_comentario',datos);
		},
		agregarCalificacion:function(datos){
			return $http.post('/avances_calificacion',datos);
		},
		getAvancesUsuario:function(datos){
			return $http.post('/avances_usuario',datos);
		},
		crearAvance:function(datos){
			return $http.post('/avances_crear',datos);
		},
		habilitarActividad:function(datos){
			return $http.post('/avances_habilitar',datos);
		},
		completarActividad:function(datos){
			return $http.post('/avances_completar',datos);
		},
	}
});