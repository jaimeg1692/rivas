angular.module('MainApp').factory('SesionesService',function($http){
	return{
		guardarSesiones:function(datos){
			return $http.post('/sesiones',datos);
		},
		getSesiones:function(){
			return $http.post('/sesiones_consulta');
		}
	}
});