angular.module('MainApp').service('toastF', function (toastr){
	
	this.success = function(msg){
		toastr.success(msg);
	},
	this.error = function(msg){
		toastr.error(msg);
	}
	this.warning = function(msg){
		toastr.warning(msg);
	}
});