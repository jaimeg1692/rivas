angular.module('MainApp').factory('UsuariosService',function($http){
	return{
		actualizarUsuario:function(datos){
			return $http.post('/usuarios_actualizar',datos);
		},
		habilitarUsuario:function(datos){
			return $http.post('/usuarios_habilitar',datos);
		},
		getUsuario:function(identificacion){
			return $http.post('/usuarios_consultar',identificacion);
		},
		getUsuariosHabilitados:function(datos){
			return $http.post('/usuarios_getUsersHabilitados',datos);
		},
		get_usuarioByUserName:function(username){
			return $http.post('/usuarios_consultar2', username);
		},
		get_usuarios:function(identificacion){
			return $http.post('/usuarios_consultar_todos',identificacion);
		},		
	}
});