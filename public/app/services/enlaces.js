angular.module('MainApp').factory('EnlacesService',function($http){
	return{
		guardarEnlaces:function(datos){
			return $http.post('/enlaces',datos)
		},
		eliminarEnlace:function(enlace){
			return $http.post('/enlaces_eliminar',enlace)
		},
		getEnlaces:function(){
			return $http.get('/enlaces');
		}
	}
});