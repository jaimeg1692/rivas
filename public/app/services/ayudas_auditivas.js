angular.module('MainApp').factory('AyudasService',function($http){
	return{
		guardarAyudas:function(datos){
			return $http.post('/ayudas',datos)
		},

		eliminarAyudas:function(ayuda){
			return $http.post('/ayudas_eliminar',ayuda)
		},
		
		getayudas_auditivas:function(datos){
			return $http.post('/ayudas_consulta',datos);
		},
	}
});