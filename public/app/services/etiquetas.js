angular.module('MainApp').factory('EtiquetasService',function($http){
	return{
		guardarEtiquetas:function(datos){
			return $http.post('/etiquetas',datos)
		},
		eliminarEtiqueta:function(etiqueta){
			return $http.post('/etiquetas_eliminar',etiqueta)
		},
		getEtiquetas:function(){
			return $http.get('/etiquetas');
		}
	}
});