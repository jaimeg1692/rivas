angular.module('MainApp').factory('RehabilitacionService',function($http){
	return{
		guardarRehabilitacion:function(datos){
			return $http.post('/rehabilitaciones',datos)
		},

		eliminarRehabilitacion:function(ayuda){
			return $http.post('/rehabilitaciones_eliminar',ayuda)
		},
		
		getrehabilitacion:function(datos){
			return $http.post('/rehabilitaciones_consulta',datos);
		},
	}
});