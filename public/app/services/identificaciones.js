angular.module('MainApp').factory('IdentificacionService',function($http){
	return{
		guardarIdentificacion:function(datos){
			return $http.post('/identificaciones',datos)
		},

		eliminarIdentificacion:function(ayuda){
			return $http.post('/identificaciones_eliminar',ayuda)
		},
		
		getIdentificacion:function(datos){
			return $http.post('/identificaciones_consulta',datos);
		},
	}
});