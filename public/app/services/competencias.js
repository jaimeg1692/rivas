angular.module('MainApp').factory('CompetenciasService',function($http){
	return{
		guardarCompetencia:function(datos){
			return $http.post('/competencias',datos)
		},
		eliminarCompetencia:function(competencia){
			return $http.post('/competencias_eliminar',competencia)
		},
		getCompetencias:function(){
			return $http.get('/competencias');
		}
	}
});