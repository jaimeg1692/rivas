angular.module('MainApp').factory('AlertasService',function($http){
	return{
		guardar_alerta:function(datos){
			return $http.post('/alertas_save',datos)
		},
		getAlertas:function(dato){
			return $http.post('/alertas',dato);
		},
		eliminarAlerta:function(alerta){
			return $http.post('/alertas_delete',alerta);
		} 
	}
});