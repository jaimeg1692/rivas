angular.module('MainApp').factory('MensajesService',function($http){
	return{
		guardarMensaje:function(datos){
			return $http.post('/mensajes',datos)
		},
		getMensajes:function(dato){
			return $http.post('/mensajes_consulta',dato);
		}
	}
});