angular.module('MainApp').factory('HistoriasMedicasService',function($http){
	return{
		getHistoria:function(identificacion){
			return $http.post('/consulta_historia',identificacion);
		},
		guardarHistoria:function(datos){
			return $http.post('/historias',datos);
		},
		getUsuario:function(identificacion){
			return $http.post('/historias_ident',identificacion);
		},
		agregarAcudiente:function(datos){
			return $http.post('/acudientes_agregar',datos);
		},
		getAcudientes:function(identificacion){
			return $http.post('/acudientes_consultar',identificacion);
		},
	}
});