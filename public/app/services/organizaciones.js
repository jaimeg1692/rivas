angular.module('MainApp').factory('OrganizacionService',function($http){
	return{
		guardarOrganizacion:function(datos){
			return $http.post('/organizaciones',datos)
		},

		eliminarOrganizacion:function(ayuda){
			return $http.post('/organizaciones_eliminar',ayuda)
		},
		
		getOrganizacion:function(datos){
			return $http.post('/organizaciones_consulta',datos);
		},
	}
});