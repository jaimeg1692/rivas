angular.module('MainApp').factory('AuditoriaService',function($http){
	return{
		guardar_auditoria:function(datos){
			return $http.post('/nueva_auditoria',datos);
		},
		getAuditorias:function(dato){
			return $http.post('/auditorias',dato);
		}
	}
});