angular.module('MainApp').factory('PuntajesService',function($http){
	return{
		getPuntajes:function(username){
			return $http.post('/puntajes',username);
		},
		guardarPuntajes:function(datos){
			return $http.post('/puntajes_guardar',datos);
		}
	}
});