  angular.module('MainApp').controller('puntajesCtrl',function($scope,$stateParams,$filter,$rootScope,toastF,Session,HabilidadesService,EnlacesService,PuntajesService,HistoriasMedicasService,UsuariosService){

  $scope.listaPuntajes=[];

  $scope.listaHabilidades = [];

  $scope.listaActividades = [];

  $scope.listaActPorHab = [];

  $scope.puntajesActPorHab = [];

  $scope.currentUser = null;

  $scope.divUsuario = true;

  $scope.divConsulta = false;

  $scope.divEspecialista = false;

  $scope.actividades = [];

  $scope.listaPuntajesHabilidad = [];

  $scope.puntajes_usuarios = [];

  $scope.puntajes_excel =  [];

  $scope.habSel = null;

  HabilidadesService.getHabilidades()
  .then(function(response)
  {
    if(response.data.success)
    {
      $scope.listaHabilidades=response.data.habilidad;
      EnlacesService.getEnlaces()
        .then(function(response)
        {
          if(response.data.success)
          {
            $scope.listaActividades=response.data.enlace;
            for (var i = 0; i < $scope.listaHabilidades.length ; i++) 
            {
              var H = $scope.listaHabilidades[i];
              var nH = H.Nombre;
              var act = [];
              for (var j = 0; j < $scope.listaActividades.length; j++) 
              {
                if( angular.equals( $scope.listaActividades[j].Habilidad.Nombre , nH ) )
                {
                  act.push( $scope.listaActividades[j] );
                }
              };
              act = $filter('orderBy')(act,'Nivel');
              var conjunto = {Habilidad:nH,Actividades:act};
              $scope.listaActPorHab.push(conjunto);
              var conjunto = {Habilidad:nH,Actividades:act,Puntajes:[]};
            };
            $scope.puntajesActPorHab = $scope.listaActPorHab;
          }
        });
    }
  });

/* Vista de puntajes usuario */
Session.getUsuario()
    .then(function(usuario)
    {
      $scope.currentUser = usuario.data.user.user;
      if(angular.equals($scope.currentUser.perfil,"USER"))
      {
        $scope.divConsulta = true;
        $scope.divUsuario = false;
        $scope.ver_puntajes($scope.currentUser);
      }
      else
      {
        $scope.divUsuario = true;
        $scope.divConsulta = false;
        $scope.divEspecialista = true;
      }   
    }
  );

/* Lista todos los usuarios */
UsuariosService.get_usuarios({})
  .then(function(response){
    if(response.data.success)
    {    
      $scope.listUsuariosUser = response.data.usuario;
      $scope.listUsuarios = $filter('filter')($scope.listUsuariosUser,{perfil:'USER'})
      $scope.listUsuarios = $filter('filter')($scope.listUsuarios,{habilitado:'true'});
    }
  });

  $scope.get_usuario=function(){
  var ident = $scope.identificacion;
  HistoriasMedicasService.getUsuario({identificacion:ident})
  .then(function(response){
    if(response.data.success&&response.data.usuario.habilitado&&angular.equals(response.data.usuario.perfil,"USER"))
    {   
      $scope.currentUser = response.data.usuario;
      toastF.success('El usuario está en el registro!')
      $scope.ver_puntajes($scope.currentUser);
      $scope.divConsulta = true;
      $scope.divUsuario = false;
    }
    else
    {
      if( response.data.success )
      {
        if( !response.data.usuario.habilitado )
        {
          toastF.error('El usuario no está habilitado!')
        }
        else
        {
          toastF.error('A este usuario no se puede crear una historia clínica!')
        }
      }
      else
      {
        toastF.error('El usuario no está en el resgitro!')
      }
    }

    });
  }

/* Muestra el puntaje del usuario seleccionado */
/*$scope.ver_puntajes=function(usuario)
{
  $scope.currentUser = usuario;
  $scope.divConsulta = true;
  PuntajesService.getPuntajes({username:usuario.nombre_usuario})
  .then(function(response)
  {
    var calHabilidad = [];
    $scope.puntajesActPorHab = $scope.listaActPorHab;
    $scope.listaPuntajes = response.data.puntaje;
    for (var i = 0; i < $scope.listaActPorHab.length; i++) 
    {
      var lA = $scope.listaActPorHab[i].Actividades;
      var sumHabilidad = 0;
      for (var j = 0; j < lA.length; j++) 
      {
        var a = lA[j].Titulo;
        var pA = $filter('filter')($scope.listaPuntajes,{actividad:a});
        $scope.puntajesActPorHab[i].Actividades[j].Puntajes = pA;
        var prom = 0;
        if( pA.length != 0 )
        {
          var puntos = pA.map(function(a){return a.puntos});
          prom = (($scope.sum(pA,'puntos')/pA.length)/arrayMax(puntos))*100;
        }
        sumHabilidad += prom;
      };
      calHabilidad.push(sumHabilidad/$scope.listaActPorHab.length);
      //console.log(sumHabilidad+"/"+$scope.listaActPorHab.length);
      console.log(puntos);
    };
    console.log(calHabilidad);

    $scope.data_general = {
      labels : ['Detección','Discriminación','Identificación','Reconocimiento','Comprensión','Evaluación Inicial'],
      datasets : [
        {
          label: "Evaluación actividades",
                  backgroundColor: [ 
                "rgba(234,7,124,0.6)",
                "rgba(167,25,109,0.6)",
                "rgba(235,36,36,0.6)",
                "rgba(0,0,180,0.6)",
                "rgba(51,51,51,0.6)"
            ],
          borderColor: "trasnparent",
          pointBackgroundColor: "#fff",
          pointBorderColor: "#A7196D",
          pointHoverBackgroundColor: "#fff",
          pointHoverBorderColor: "#A7196D",
          data : calHabilidad
        }
      ]
    };
  });
}

  $scope.vista_actividades=function(actividad)
  {
    actividad.estado = !actividad.estado;
  }*/

  $scope.ver_puntajes=function(usuario)
  {
  $scope.currentUser = usuario;
  $scope.divConsulta = true;
  PuntajesService.getPuntajes({username:usuario.nombre_usuario})
  .then(function(response)
  {
    var calHabilidad = [];
    $scope.puntajesActPorHab = $scope.listaActPorHab;
    $scope.listaPuntajes = response.data.puntaje;
    for (var i = 0; i < $scope.listaActPorHab.length; i++) 
    {
      var lA = $scope.listaActPorHab[i].Actividades;
      var sumHabilidad = 0;
      var puntos_posibles=0;
      for (var j = 0; j < lA.length; j++) 
      {
        var a = lA[j].Titulo;
        var pA = $filter('filter')($scope.listaPuntajes,{actividad:a});
        $scope.puntajesActPorHab[i].Actividades[j].Puntajes = pA;
        puntos_posibles=$scope.puntajesActPorHab[i].Actividades[j].Puntos; 
        var prom = 0;
        if( pA.length != 0 )
        {
          var puntos = pA.map(function(a){return a.puntos});
          //prom = (($scope.sum(pA,'puntos')/pA.length)/arrayMax(puntos))*100;
          prom = (($scope.sum(pA,'puntos')/pA.length)/puntos_posibles)*100;
          //console.log(suma_puntos_posibles); 
          console.log($scope.sum(pA,'puntos')+"/"+puntos_posibles)
        }
        sumHabilidad += prom;
      };
      calHabilidad.push(sumHabilidad/$scope.listaActPorHab.length);
    };

    $scope.data_general = {
      labels : ['Detección','Discriminación','Identificación','Reconocimiento','Comprensión','Evaluación Inicial'],
      datasets : [
        {
          label: "Evaluación actividades",
                  backgroundColor: [ 
                "rgba(234,7,124,0.6)",
                "rgba(167,25,109,0.6)",
                "rgba(235,36,36,0.6)",
                "rgba(0,0,180,0.6)",
                "rgba(51,51,51,0.6)"
            ],
          borderColor: "trasnparent",
          pointBackgroundColor: "#fff",
          pointBorderColor: "#A7196D",
          pointHoverBackgroundColor: "#fff",
          pointHoverBorderColor: "#A7196D",
          data : calHabilidad
        }
      ]
    };
  });
}

  /* Habilidades */
  $scope.habilidad_seleccionada = function(habilidad)
  {
  	console.log($scope.puntajesActPorHab);
    $scope.habSel = habilidad;
    var obj = $filter('filter')($scope.puntajesActPorHab,{Habilidad:habilidad.Nombre});
    $scope.actividades = obj[0].Actividades;
    var puntajesHabilidad = $scope.actividades.map(function(a){return a.Puntajes});
    $scope.listaPuntajesHabilidad = [].concat.apply([], puntajesHabilidad);
    angular.forEach($scope.actividades, function (actividad, index){  
          $scope.resultado = actividad.Puntajes;
          var listaPuntos = $scope.resultado.map(function(a){return a.puntos});
          var puntos_posibles=Array.apply(null, Array(listaPuntos.length)).map(Number.prototype.valueOf,actividad.Puntos);
          //var puntos_posibles=Array.apply(null, Array(listaPuntos.length)).map(Number.prototype.valueOf,10);
          console.log(actividad.Puntos);
          var puntos_negativos=[];
          for(var i=0; i<listaPuntos.length;i++){
          	 puntos_negativos[i]=puntos_posibles[i]-listaPuntos[i];
          }

          var nombreGrafica={
              labels : intentos(listaPuntos.length),
              datasets : [
                {
                  label: "Puntos Obtenidos en Actividad "+actividad.Titulo,
                  backgroundColor: "rgba(234,7,124,0.2)",
                  borderColor: "#EA077C",
                  pointBackgroundColor: "#fff",
                  pointBorderColor: "#A7196D",
                  pointHoverBackgroundColor: "#fff", 
                  pointHoverBorderColor: "#A7196D",
                  data : listaPuntos
                }
                ,{
                  label: "Puntos No Obtenidos en Actividad "+actividad.Titulo,
                  backgroundColor: "rgba(75,75,75,0.2)",
                  borderColor: "#555",
                  pointBackgroundColor: "#eee",
                  pointBorderColor: "#000",
                  pointHoverBackgroundColor: "#aaa",
                  pointHoverBorderColor: "#aaa",
                  data : puntos_negativos
                },

              ]
          };
          actividad["nombreGrafica"]=nombreGrafica;
          actividad["estado"]=true;
        });  
  }

  $scope.crear_puntajes=function(){
  PuntajesService.guardarPuntajes(
    {
      nombreUsuario:$scope.nuevo.usuario.nombre_usuario,
      puntos:$scope.nuevo.puntos,
      nombreActividad:$scope.nuevo.actividad,
      tiempo:$scope.nuevo.tiempo
    })
  .then(function(response){
    if(response.data.success)
    {   
      toastF.success('Se creo el puntaje!')
    }
    else
    {
      toastF.error('No se creo el puntaje!')
    }
    });
  }

  $scope.regresar_usuario=function()
  {
    $scope.currentUser = null;
    $scope.divUsuario = true;
    $scope.divConsulta = false;
  }

  $scope.sum = function(items,prop)
  {
    return items.reduce( function(a,b){return a + b[prop];}, 0);
  };

  function arrayMax(items) 
  {
    return items.reduce(function(a, b) {
      return Math.max(a, b);
    });
  };

  function intentos(n)
  {
    var res = [];
    for (var i = 1; i <= n; i++) {
      res.push(i);
    };
    return res;
  };

  angular.element(document).ready(function(){
    UsuariosService.get_usuarios().then( function(res) {
    var usuarios=res.data; 
  	for(var i=0;i<usuarios.usuario.length;i++){
        if (usuarios.usuario[i].perfil === "USER") {
  	    	  PuntajesService.getPuntajes({username:usuarios.usuario[i].nombre_usuario}).then(function(response){
            if(response.data.success){
              var puntajes = [].concat.apply([],response.data.puntaje);
              $scope.puntajes_excel.push(puntajes);
            } else {
              console.log("Error");
            }
  	    	});
        }
  	   }
    });
  });

  $scope.obtener_puntajes_completos=function()
  {
    $scope.puntajes_usuarios = [].concat.apply([],$scope.puntajes_excel);
    return  $scope.puntajes_usuarios;
  }

});