angular.module('MainApp').controller('puntajesCtrl',function($scope,$stateParams,$filter,$rootScope,toastF,Session,HabilidadesService,EnlacesService,PuntajesService,HistoriasMedicasService,UsuariosService){

/* Parametros del modulo */

/*  var checkeados=[];
  $scope.detecciones=[];
  $scope.resultados=[];
  $scope.enlaces_materia=[];
  var resultadosBosque=[];
  var resultadosBosqueEnteros=[];
  var resultadosBosqueStrings=[];
  var resultadosCasa=[];
  var resultadosCasaEnteros=[];
  var resultadosCasaStrings=[];
  var resultadosAeropuerto=[];
  var resultadosAeropuertoEnteros=[];
  var resultadosAeropuertoStrings=[];
  var labelCasa=[];
  var labelAeropuerto=[];
  var labelBosque=[];
  var labelBosqueY=["PUNTOS"];*/

  $scope.listaHabilidades = [];

  $scope.listaActividades = [];

  $scope.listaActPorHab = [];

  $scope.puntajesActPorHab = [];

  $scope.currentUser = null;

  $scope.divUsuario = true;

  $scope.divConsulta = false;

  HabilidadesService.getHabilidades()
  .then(function(response)
  {
    if(response.data.success)
    {
      $scope.listaHabilidades=response.data.habilidad;
      EnlacesService.getEnlaces()
        .then(function(response)
        {
          if(response.data.success)
          {
            $scope.listaActividades=response.data.enlace;
            for (var i = 0; i < $scope.listaHabilidades.length ; i++) 
            {
              var H = $scope.listaHabilidades[i];
              var nH = H.Nombre;
              var act = [];
              for (var j = 0; j < $scope.listaActividades.length; j++) 
              {
                if( angular.equals( $scope.listaActividades[j].Habilidad.Nombre , nH ) )
                {
                  act.push( $scope.listaActividades[j] );
                }
              };
              act = $filter('orderBy')(act,'Nivel');
              var conjunto = {Habilidad:nH,Actividades:act};
              $scope.listaActPorHab.push(conjunto);
              var conjunto = {Habilidad:nH,Actividades:act,Puntajes:[]};
            };
            $scope.puntajesActPorHab = $scope.listaActPorHab;
            console.log($scope.listaActPorHab);
          }
        });
    }
  });

/* Vista de puntajes usuario */
Session.getUsuario()
    .then(function(usuario)
    {
      $scope.currentUser = usuario.data.user.user;
      if(angular.equals($scope.currentUser.perfil,"USER"))
      {
        $scope.divConsulta = true;
        $scope.divUsuario = false;
        PuntajesService.getPuntajes({username:$scope.currentUser.nombre_usuario})
          .then(function(response){
            var listaPuntajes = [];
            for (var i = 0; i < $scope.listaActividades.length; i++) 
            {
              var nA = $scope.listaActividades[i].Titulo;
              var pA = $filter('filter')(response.data,{actividad:nA});
              var conjunto = {Actividad:nA,Puntajes:pA};
              listaPuntajes.push(conjunto);
            };
            for (var i = 0; i < $scope.listaActPorHab.length; i++) 
            {
              var lA = $scope.listaActPorHab[i].Actividades;
              for (var j = 0; j < lA.length; j++) 
              {
                var a = lA[j].Titulo;
                for (var k = 0; k < listaPuntajes.length; k++) {
                  var alP = listaPuntajes[k].Actividad;
                  if(angular.equals(a,alP))
                  {
                    $scope.puntajesActPorHab[i].Actividades[j].Puntajes = null;
                  }
                };
              };
            };
            console.log($scope.puntajesActPorHab);
/*            _.each(response.data, function(item){
              $scope.resultadosCasa = $filter('filter')(response.data,{actividad:'Deteccion casa'});
              $scope.resultadosAeropuerto = $filter('filter')(response.data,{actividad:'Deteccion aeropuerto'});
              $scope.resultadosBosque = $filter('filter')(response.data,{actividad:'Deteccion bosque'});
              
              resultadosBosqueStrings= $scope.resultadosBosque.map(function(a) {return a.puntos;});
              for(var i=0; i<resultadosBosqueStrings.length;i++) {
                labelBosque[i]="Intento "+" "+(i+1);
                resultadosBosqueEnteros[i] = parseInt(resultadosBosqueStrings[i], 10);
              }
              $scope.detecciones.push(resultadosCasa);
              
              resultadosCasaStrings= $scope.resultadosCasa.map(function(a) {return a.puntos;});
              for(var i=0; i<resultadosCasaStrings.length;i++){
                labelCasa[i]="Intento "+" "+(i+1);
                resultadosCasaEnteros[i] = parseInt(resultadosCasaStrings[i], 10);
              } 

              resultadosAeropuertoStrings= $scope.resultadosAeropuerto.map(function(a) {return a.puntos;});
              for(var i=0; i<resultadosAeropuertoStrings.length;i++){
                labelAeropuerto[i]="Intento "+" "+(i+1);
                resultadosAeropuertoEnteros[i] = parseInt(resultadosAeropuertoStrings[i], 10);
              }

            });*/  
          });
      }
      else
      {
        $scope.divUsuario = true;
      }   
    }
  );

/* Lista todos los usuarios */
UsuariosService.get_usuarios({})
  .then(function(response){
    if(response.data.success)
    {    
      $scope.listUsuariosUser = response.data.usuario;
      $scope.listUsuarios = $filter('filter')($scope.listUsuariosUser,{perfil:'USER'});
    }
  });

/* Muestra el puntaje del usuario seleccionado */
$scope.ver_puntajes=function(usuario)
  {
  var ident = usuario.identificacion;
  HistoriasMedicasService.getUsuario({identificacion:ident})
  .then(function(response){
    if(response.data.success&&response.data.usuario.habilitado&&angular.equals(response.data.usuario.perfil,"USER"))
    {   
      var j=0
      $scope.currentUser = response.data.usuario;
      toastF.success('El usuario está en el registro!')
      $scope.divConsulta = true;

      HabilidadesService.getHabilidades()
      .then(function(response)
      {
        if(response.data.success)
        { 
          angular.forEach(response.data.habilidad, function (habilidad, index) {
            EnlacesService.getEnlaces()
            .then(function(response){
              if(response.data.success){
                $scope.enlaces_materia = $filter('filter')(response.data.enlace,{Habilidad:habilidad});
                  /*------------------------------------------*/
                  PuntajesService.getPuntajes({username:$scope.currentUser.nombre_usuario})
                  .then(function(response){
                    _.each(response.data, function(item){
                        angular.forEach($scope.enlaces_materia, function (habilidad, index) {
                          var resultado="resultado_"+habilidad.Titulo.replace(/ /g,"_");
                          var resultadosEnteros="resultados_"+habilidad.Titulo.replace(/ /g,"_")+"_Enteros";
                          $scope.resultado = $filter('filter')(response.data,{actividad:habilidad.Titulo});
                          resultadoStrings= $scope.resultado.map(function(a) {return a.puntos;});

                          for(var i=0; i<resultadoStrings.length;i++) {
                              labelBosque[i]="Intento "+" "+(i+1);  
                              resultadosEnteros[i] = parseInt(resultadoStrings[i], 10);
                          } 

                          resultadosTotalesActividad[j]=resultadosEnteros/resultadoStrings.length;
                          j++;
                          console.log("ewfijwetowaoetjweojirowejfojawo");
                          console.log(j);
                          /* Datos puntajes generales */
                          $scope.data_general = {
                            labels : [ 'Detección','Discriminación','Reconocimiento','Identificación','Comprensión'],
                            datasets : [
                              {
                                label: "Evaluación actividades",
                                        backgroundColor: [
                                      "rgba(234,7,124,0.6)",
                                      "rgba(167,25,109,0.6)",
                                      "rgba(235,36,36,0.6)",
                                      "rgba(0,0,180,0.6)",
                                      "rgba(51,51,51,0.6)"
                                  ],
                                borderColor: "trasnparent",
                                pointBackgroundColor: "#fff",
                                pointBorderColor: "#A7196D",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "#A7196D",
                                data : [ 70, 90, 20, 40, 50 ]
                              }
                            ]
                          };  
                        });    
                    });  
                  });
                  /*------------------------------------------*/
              }
            });
          });
        }
      });      
    }
    else
    {
      if( response.data.success)
      {
        if( !response.data.usuario.habilitado )
        {
          toastF.error('El usuario no está habilitado!')
        }
        else
        {
          toastF.error('A este usuario no se puede crear una historia clínica!')
        }
      }
      else
      {
        toastF.error('El usuario no está en el resgitro!')
      }
    }

    });
  }

/* Muestra el puntaje del usuario seleccionado 
$scope.get_usuario=function(){
  var ident = $scope.identificacion;
  HistoriasMedicasService.getUsuario({identificacion:ident})
  .then(function(response){
    if(response.data.success&&response.data.usuario.habilitado&&angular.equals(response.data.usuario.perfil,"USER"))
    {   
      $scope.currentUser = response.data.usuario;
      toastF.success('El usuario está en el registro!')
      consultar_resultados(response.data.usuario.nombre_usuario);
    }
    else
    {
      if( response.data.success )
      {
        if( !response.data.usuario.habilitado )
        {
          toastF.error('El usuario no está habilitado!')
        }
        else
        {
          toastF.error('A este usuario no se puede crear una historia clínica!')
        }
      }
      else
      {
        toastF.error('El usuario no está en el resgitro!')
      }
    }

    });
  }
  */

   $scope.vista_actividades=function(enlace)
   {
    enlace.estado= !enlace.estado;
    console.log(enlace.estado);
  }

  /* Habilidades */
  $scope.habilidad_seleccionada = function(Nombre){
    getEnlaces(Nombre);
  }

  HabilidadesService.getHabilidades()
  .then(function(response)
  {
    if(response.data.success)
    {
      $scope.listaHabilidades=response.data.habilidad;
      EnlacesService.getEnlaces()
        .then(function(response)
        {
          if(response.data.success)
          {
            $scope.listaActividades=response.data.enlace;
          }
        });
    }
  });

  /* Enlaces */
  getEnlaces = function(habilidad){
    EnlacesService.getEnlaces()
    .then(function(response){
      if(response.data.success){
          $scope.enlaces_materia = $filter('filter')(response.data.enlace,{Habilidad:habilidad});
          angular.forEach($scope.enlaces_materia, function (enlaces, index) {  
            var nombreGrafica={
                labels : labelAeropuerto,
                datasets : [
                  {
                    label: "Puntos Obtenidos en Actividad "+enlaces.Titulo,
                    backgroundColor: "rgba(234,7,124,0.2)",
                    borderColor: "#EA077C",
                    pointBackgroundColor: "#fff",
                    pointBorderColor: "#A7196D",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "#A7196D",
                    data : "resultados_"+enlaces.Titulo.replace(/ /g,"_")+"_Enteros"
                  }
                ]
            };
            enlaces["nombreGrafica"]=nombreGrafica;
            enlaces["estado"]=true;
            PuntajesService.getPuntajes({username:$scope.currentUser.nombre_usuario})
            .then(function(response){
              _.each(response.data, function(item){
                $scope.divConsulta = true;
                var resultado="resultado_"+enlaces.Titulo.replace(/ /g,"_");
                var resultadosEnteros="resultados_"+enlaces.Titulo.replace(/ /g,"_")+"_Enteros";
                $scope.resultado = $filter('filter')(response.data,{actividad:enlaces.Titulo});
                resultadoStrings= $scope.resultado.map(function(a) {return a.puntos;});
                for(var i=0; i<resultadoStrings.length;i++) {
                  labelBosque[i]="Intento "+" "+(i+1);
                  resultadosEnteros[i] = parseInt(resultadoStrings[i], 10);
                }
              });  
            });
        });
      }
    });
  }
});