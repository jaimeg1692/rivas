angular.module('MainApp').controller('auditoriaCtrl',function($scope,$stateParams,Session,AuditoriaService,toastF){
    
    $scope.auditorias = [];
    $scope.ocultar=true;

    AuditoriaService.getAuditorias()
    .then(function(response){
      if(response.data.success){      
        $scope.auditorias=response.data.auditoria;
      }
    });

    Session.getUsuario()
    .then(function(response){
      $scope.usuario = response.data.user.user;
      if((response.data.user.user.perfil=="ADMIN") || (response.data.user.user.perfil=="ESPE")){
        $scope.ocultar=false; 
      }else{
        $scope.ocultar=true;
      }      
    });

});